## Virginia Tech Enterprise Directory Java libraries

This project is dual licensed under both the LGPL and Apache 2.
If you have questions or comments about this library contact Middleware Services (middleware@vt.edu).

### Building
```sh
git clone git@code.vt.edu:middleware/edldap.git
cd edldap
mvn package
```

### Documentation
See the wiki: https://code.vt.edu/middleware/edldap/wikis/home

### Testing
The LOCAL environment is active when running integration tests against the edldap-deploy-env docker image.

`src/test/resources/ldap-keys` contains cert and key material for "bringing your own certs" to the docker image in place
of what is already in the image. This is necessary for setting up trust and authn.

`src/test/resources/ldif` contains the ldif to be loaded upon starting the edldap-deploy-env docker image.

To run the integration tests: `mvn verify`\
Test users can be found in the ldif being loaded, and additional users can be added to the ldif if necessary.

