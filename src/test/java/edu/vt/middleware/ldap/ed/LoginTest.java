/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.Credential;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

/**
 * Unit tests for multi-factor implementations.
 *
 * @author  Middleware Services
 */
public class LoginTest
{

  /**
   * Unit test for {@link Login}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "logintest")
  public void testEdAuthMFA()
          throws Exception
  {
    final Login edmfa = new Login();
    AssertJUnit.assertEquals(edmfa.getType(), DirectoryType.LOGIN);
  }

  /**
   * Unit tests for {@link TokensFactor}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "logintest")
  public void testTokensFactor()
          throws Exception
  {
    final String password = "myPassword";
    final Credential credential = new Credential(password);
    Throwable e = null;
    try {
      new TokensFactor().getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    AssertJUnit.assertNull(e);
    //Assert getValue equals provided credentials
    AssertJUnit.assertEquals(password, new TokensFactor().getValue(credential).getString());
  }

  /**
   * Unit tests for {@link DuoFactor}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "logintest")
  public void testDuoFactor()
          throws Exception
  {
    final String password = "myPassword";
    final Credential credential = new Credential(password);
    Throwable e = null;
    try {
      new DuoFactor(DuoFactor.Type.PASSCODE).getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //Passcode must have value
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.PASSCODE, 13).getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //Passcode cannot have index
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.AUTO, 13).getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //Auto cannot have index
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.AUTO, "value").getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //Auto cannot have value
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.SMS, "value").getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //SMS cannot have value
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.PHONE, "value").getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //PHONE cannot have value
    AssertJUnit.assertNotNull(e);

    e = null;
    try {
      new DuoFactor(DuoFactor.Type.PUSH, "value").getValue(credential);
    } catch (IllegalArgumentException ex) {
      e = ex;
    }
    //PUSH cannot have value
    AssertJUnit.assertNotNull(e);

    AssertJUnit.assertEquals(password + ",phone",
            new DuoFactor(DuoFactor.Type.PHONE).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",phone",
            new DuoFactor(DuoFactor.Type.PHONE, 0).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",phone1",
            new DuoFactor(DuoFactor.Type.PHONE, 1).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",push",
            new DuoFactor(DuoFactor.Type.PUSH).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",push",
            new DuoFactor(DuoFactor.Type.PUSH, 0).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",push1",
            new DuoFactor(DuoFactor.Type.PUSH, 1).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",auto",
            new DuoFactor(DuoFactor.Type.AUTO).getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",12345",
            new DuoFactor(DuoFactor.Type.PASSCODE, "12345").getValue(credential).getString());
    AssertJUnit.assertEquals(password + ",sms2",
            new DuoFactor(DuoFactor.Type.SMS, 2).getValue(credential).getString());

  }

}
