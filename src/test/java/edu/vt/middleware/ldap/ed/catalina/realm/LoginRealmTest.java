/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.catalina.realm;

import java.security.Principal;
import edu.vt.middleware.ldap.ed.DirectoryEnv;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link LoginRealm}.
 *
 * @author  Middleware Services
 */
public class LoginRealmTest
{

  /** Auth ID used for all tests. */
  private String authId;

  /** Password for authId. */
  private String password;

  /** Subject of test. */
  private LoginRealm loginRealm;

  /**
   * @param  user  to auth with.
   * @param  pass  to auth with.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters(
    {
      "edauth-authId",
      "edauth-password"
    })
  @BeforeClass(groups = "edrealmtest")
  public void loadCredentials(final String user, final String pass)
  {
    authId = user;
    password = pass;
  }

  /**
   * @param  env  environment.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edrealmtest")
  public void createLdap(final DirectoryEnv env)
    throws Exception
  {
    loginRealm = new LoginRealm(env);
  }

  /**
   * Pos test method for {@link LoginRealm#authenticate(String, String)}
   */
  @Test(groups = "edrealmtest")
  public void authenticatePos()
  {
    final Principal principal = loginRealm.authenticate(authId, password);
    AssertJUnit.assertEquals(authId, principal.getName());
  }

  /**
   * Neg test method for {@link LoginRealm#authenticate(String, String)}
   */
  @Test(groups = "edrealmtest")
  public void authenticateNeg()
  {
    final Principal principal = loginRealm.authenticate(authId, "wrong-pass");
    AssertJUnit.assertNull(principal);
  }
}
