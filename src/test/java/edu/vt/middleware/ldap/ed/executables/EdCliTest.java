/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.executables;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EdCli}.
 *
 * @author  Middleware Services
 */
public class EdCliTest
{

  /** Auth ID used for all tests. */
  private String authId;

  /** Password for authId. */
  private String password;

  /** LDAP authorization expression. */
  private String authorizationExpression;

  /** ED Environment to test with. */
  private String environment;


  /**
   * @param  user  to bind with.
   * @param  pass  to bind with.
   * @param  ae  authorization expression.
   * @param  env  environment.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters(
    {
      "edauth-authId",
      "edauth-password",
      "edcli-authorizationExpression",
      "environment"
    })
  @BeforeClass(groups = "edclitest")
  public void loadCredentials(final String user, final String pass, final String ae, final String env)
    throws Exception
  {
    authId = user;
    password = pass;
    authorizationExpression = ae;
    environment = env;
  }

  /**
   * Test method for {@link EdCli}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "edclitest")
  public void testEDClientInterface()
    throws Exception
  {
    EdCli.main(
      new String[] {
        "-env",
        environment,
        "-authenticate",
        "-showaffils",
        "-showgroups",
        "-user",
        authId,
        "-credential",
        password,
      });
    /* edldap docker image doesn't support DUO MFA
    EdCli.main(
      new String[] {
        "-env",
        environment,
        "-authenticate",
        "-user",
        authId,
        "-showaffils",
        "-showgroups",
        "-credential",
        password,
        "-mfa",
        "DUO",
        "-mfaval",
        "auto",
        "-authorize",
        authorizationExpression,
      });
     */
    EdCli.main(
      new String[] {
        "-env",
        environment,
        "-user",
        authId,
        "-credential",
        password,
        "-authorize",
        authorizationExpression,
      });
  }
}
