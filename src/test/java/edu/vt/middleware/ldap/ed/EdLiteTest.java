/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import edu.vt.middleware.ldap.ed.beans.VirginiaTechGroup;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.LdapEntry;
import org.ldaptive.SearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EdLite}.
 *
 * @author  Middleware Services
 */
public class EdLiteTest
{

  /** Class logger instance. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EdLiteTest.class);

  /** Subject of test. */
  private EdLite edlite;


  /**
   * @param  env  to search.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edlitetest")
  public void createLdap(final String env)
    throws Exception
  {
    edlite = new EdLite(DirectoryEnv.valueOf(DirectoryEnv.class, env));
  }

  /**
   * Test method for {@link EdId#getVirginiaTechPerson(java.lang.String)}.
   *
   * @param  authId  to get VirginiaTechPerson with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edlite-virginiatechperson-authId")
  @Test(groups = "edlitetest")
  public void testGetVirginiaTechPerson(final String authId)
    throws Exception
  {
    final VirginiaTechPerson vtp = edlite.getVirginiaTechPerson(authId);

    AssertJUnit.assertNotNull(vtp);
    AssertJUnit.assertNotNull(vtp.getUid());
  }

  /**
   * Test method for {@link EdId#getVirginiaTechGroup(java.lang.String)}.
   *
   * @param  uugidOrDn  to get VirginiaTechGroup with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edlite-virginiatechgroup-uugid")
  @Test(groups = "edlitetest")
  public void testGetVirginiaTechGroup(final String uugidOrDn)
    throws Exception
  {
    final VirginiaTechGroup vtp = edlite.getVirginiaTechGroup(uugidOrDn);

    AssertJUnit.assertNotNull(vtp);
    AssertJUnit.assertNotNull(vtp.getUid());
    AssertJUnit.assertNotNull(vtp.getUugid());
  }


  /**
   * Test method for {@link EdLite#search(String, String[])}.
   *
   * @param  query  to execute
   * @param  returnAttrs  to retrieve from the search
   *
   * @throws  Exception  On test errors.
   */
  @Parameters({ "edlite-query", "edlite-returnAttrs" })
  @Test(groups = "edlitetest")
  public void testSearch(final String query, final String returnAttrs)
    throws Exception
  {
    int count = 0;
    final SearchResponse searchResult = edlite.search(query, returnAttrs.split("\\|"));

    LOGGER.debug("Attributes:");
    for (LdapEntry entry : searchResult.getEntries()) {
      LOGGER.debug("  {}", entry);
      count++;
    }

    AssertJUnit.assertTrue(count > 0);
  }
}
