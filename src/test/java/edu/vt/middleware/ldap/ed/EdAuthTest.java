/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.Credential;
import org.ldaptive.LdapEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EdAuth}.
 *
 * @author  Middleware Services
 */
public class EdAuthTest
{

  /** Class logger instance. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EdAuthTest.class);

  /** Auth ID used for all tests. */
  private String authId;

  /** Password for authId. */
  private Credential password;

  /** LDAP authorization attribute. */
  private String authorizationAttribute;

  /** Another LDAP authorization attribute. */
  private String authorizationAttribute2;

  /** LDAP authorization expected value to search and match. */
  private String authorizationAttributeMatch;

  /** Subject of test. */
  private EdAuth edauth;


  /**
   * @param  user  to bind with.
   * @param  pass  to bind with.
   * @param  aa  authorization attribute.
   * @param  aa2  another authorization attribute.
   * @param  aam  authorization attribute match.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters(
    {
      "edauth-authId",
      "edauth-password",
      "edauth-authorizationAttribute",
      "edauth-authorizationAttribute2",
      "edauth-authorizationAttributeMatch"
    })
  @BeforeClass(groups = "edauthtest")
  public void loadCredentials(final String user, final String pass, final String aa, final String aa2, final String aam)
    throws Exception
  {
    authId = user;
    password = new Credential(pass);
    authorizationAttribute = aa;
    authorizationAttribute2 = aa2;
    authorizationAttributeMatch = aam;
  }


  /**
   * @param  env  to search.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edauthtest")
  public void createLdap(final String env)
    throws Exception
  {
    edauth = new EdAuth(DirectoryEnv.valueOf(DirectoryEnv.class, env));
  }


  /**
   * Test method for {@link EdAuth#authenticate(String, Credential, String[])}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "edauthtest")
  public void testAuthenticate()
    throws Exception
  {
    LOGGER.debug("Attempting to authenticate user {}", authId);
    edauth.authenticate(authId, password, (String[]) null);
  }


  /**
   * Test method for {@link EdAuth#authenticateAndGetAttributes(String, Credential, String[])}.
   *
   * @throws Exception On test errors.
   */
  @Test(groups = "edauthtest")
  public void testAuthenticateAndAuthorize()
          throws Exception
  {
    // Maybe should prompt user for filter via readLine
    // in case folks other that MW devs want to run tests?
    LOGGER.debug("Attempting to authenticate and authorize user {}", authId);
    LOGGER.debug("Auth return attribute: {}", authorizationAttribute);
    LOGGER.debug("Return attribute should contain: {}", authorizationAttributeMatch);

    final LdapEntry entry = edauth.authenticate(authId, password, authorizationAttribute, authorizationAttribute2);

    AssertJUnit.assertNotNull(entry);

    boolean authorized = false;

    for (String value : entry.getAttribute(authorizationAttribute).getStringValues()) {
      if (value.equals(authorizationAttributeMatch)) {
        authorized = true;
        break;
      }
    }

    edauth.authorize(entry, "hasAttribute('" + authorizationAttribute2 + "')");

    edauth.authorize(entry,
            "hasAttributeValue('" + authorizationAttribute + "', '" + authorizationAttributeMatch + "')");

    edauth.authorize(entry,
            "hasAttribute('" + authorizationAttribute2 + "') &&" +
            " hasAttributeValue('" + authorizationAttribute + "', '" + authorizationAttributeMatch + "')");

    edauth.authorize(entry,
            "hasAttribute('this does not exist, but the other does') ||" +
            " hasAttributeValue('" + authorizationAttribute + "', '" + authorizationAttributeMatch + "')");

    // Check for a failed authorization
    try {
      edauth.authorize(entry, "hasAttributeValue('" + authorizationAttribute + "', 'FAIL!')");
    } catch (Exception e) {
      AssertJUnit.assertEquals(EdAuthAuthorizationException.class, e.getClass());
    }

    // Check for empty authorization expression.
    try {
      edauth.authorize(entry, "");
    } catch (Exception e) {
      AssertJUnit.assertEquals(EdAuthAuthorizationException.class, e.getClass());
    }

    // Check for bad authorization expression.
    try {
      edauth.authorize(entry, "this is a bad expression");
    } catch (Exception e) {
      AssertJUnit.assertEquals(EdAuthAuthorizationException.class, e.getClass());
    }

    AssertJUnit.assertTrue(authorized);
  }


  /**
   * Test method for {@link EdAuth#getAffiliations(LdapEntry)}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "edauthtest")
  public void testGetAffiliations()
    throws Exception
  {

    final String[] affils = edauth.getAffiliations(edauth.authenticate(authId, password, EdContext.AFFILIATION_ATTR));
    LOGGER.debug("Affiliations of user {}:", authId);
    for (String affil : affils) {
      LOGGER.debug("  {}", affil);
    }

    AssertJUnit.assertTrue(affils.length > 0);

  }

  /**
   * Test method for {@link EdAuth#getVirginiaTechPerson(LdapEntry)}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "edauthtest")
  public void testGetVirginiaTechPerson()
    throws Exception
  {

    final VirginiaTechPerson vtp = edauth.getVirginiaTechPerson(
            edauth.authenticate(authId, password, EdContext.ALL_ATTR));

    LOGGER.debug("VirginiaTechPerson retrieved: {}", vtp);
    AssertJUnit.assertNotNull(vtp);

    AssertJUnit.assertNotNull(vtp.getEduPersonAffiliation());
    AssertJUnit.assertNotNull(vtp.getEduPersonPrimaryAffiliation());
    AssertJUnit.assertNotNull(vtp.getVirginiaTechAffiliation());
    AssertJUnit.assertNotNull(vtp.getUid());

  }


  /**
   * Test method for {@link EdAuth#getGroupMembership(LdapEntry)}.
   *
   * @throws  Exception  On test errors.
   */
  @Test(groups = "edauthtest")
  public void testGetGroupMembershipStringObject()
    throws Exception
  {
    final String[] groups = edauth.getGroupMembership(
            edauth.authenticate(authId, password, EdContext.GROUP_MEMBERSHIP_ATTR));
    LOGGER.debug("Groups of which {} is a member:", authId);
    for (String group : groups) {
      LOGGER.debug("  {}", group);
    }
    AssertJUnit.assertTrue(groups.length > 0);
  }
}
