/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.jetty.realm;

import java.security.Principal;
import javax.security.auth.Subject;
import javax.servlet.ServletRequest;
import edu.vt.middleware.ldap.ed.DirectoryEnv;
import org.eclipse.jetty.server.UserIdentity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link LoginRealm}.
 *
 * @author  Middleware Services
 */
public class LoginRealmTest
{

  /** Auth ID used for all tests. */
  private String authId;

  /** Password for authId. */
  private String password;

  /** Subject of test. */
  private LoginRealm loginRealm;

  /**
   * @param  user  to auth with.
   * @param  pass  to auth with.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters(
    {
      "edauth-authId",
      "edauth-password"
    })
  @BeforeClass(groups = "edrealmtest")
  public void loadCredentials(final String user, final String pass)
  {
    authId = user;
    password = pass;
  }

  /**
   * @param  env  environment.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edrealmtest")
  public void init(final DirectoryEnv env)
    throws Exception
  {
    loginRealm = new LoginRealm(env);
  }

  /**
   * Post test method for {@link EdAuthRealm#login(String, Object, ServletRequest)}
   */
  @Test(groups = "edrealmtest")
  public void loginPos()
  {
    final MockHttpServletRequest request = new MockHttpServletRequest();
    final UserIdentity userIdentity = loginRealm.login(authId, password, request);
    AssertJUnit.assertEquals(authId, userIdentity.getUserPrincipal().getName());
  }

  /**
   * Neg test method for {@link EdAuthRealm#login(String, Object, ServletRequest)}
   */
  @Test(groups = "edrealmtest")
  public void loginNeg()
  {
    final MockHttpServletRequest request = new MockHttpServletRequest();
    final UserIdentity userIdentity = loginRealm.login(authId, "wrong-pass", request);
    AssertJUnit.assertNull(userIdentity);
  }

  /**
   * Test method for {@link EdAuthRealm#validate(UserIdentity)}
   */
  @Test(groups = "edrealmtest")
  public void validate()
  {
    final UserIdentity userIdentity = new UserIdentity()
    {
      @Override
      public Subject getSubject()
      {
        return null;
      }

      @Override
      public Principal getUserPrincipal()
      {
        return null;
      }

      @Override
      public boolean isUserInRole(final String s, final Scope scope)
      {
        return false;
      }
    };
    AssertJUnit.assertTrue(loginRealm.validate(userIdentity));
    AssertJUnit.assertFalse(loginRealm.validate(null));
  }
}
