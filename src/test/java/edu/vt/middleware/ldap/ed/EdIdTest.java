/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechEntitlement;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechGroup;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.LdapAttribute;
import org.ldaptive.SearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EdId}.
 *
 * @author  Middleware Services
 */
public class EdIdTest
{

  /** Class logger instance. */
  private static final Logger LOGGER = LoggerFactory.getLogger(EdIdTest.class);

  /** Subject of test. */
  private EdId edid;

  /**
   * @param  env  to search.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edidtest")
  public void createLdap(final String env)
    throws Exception
  {
    edid = new EdId(DirectoryEnv.valueOf(DirectoryEnv.class, env));
  }

  /** Test method for {@link EdId#getType()}. */
  @Test(groups = "edidtest")
  public void testGetType()
  {
    AssertJUnit.assertEquals(DirectoryType.EDID, edid.getType());
  }

  /**
   * Test method for {@link EdId#getAttributes(String, String...)}.
   *
   * @param  authId  to search on
   * @param  dn  to compare with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters({ "edid-attributes-authId", "edid-attributes-Dn" })
  @Test(groups = "edidtest")
  public void testGetAttributes(final String authId, final String dn)
    throws Exception
  {
    final String testDn = edid.getDn(authId);
    AssertJUnit.assertEquals(dn, testDn);

    final Collection<LdapAttribute> attrs = edid.getAttributes(testDn, (String[]) null);
    LOGGER.debug("testGetAttributes attributes: {}", attrs);
    AssertJUnit.assertTrue(attrs.size() > 0);
  }

  /**
   * Test method for {@link EdId#search(String, String[])}.
   *
   * @param  query  to search with
   * @param  returnAttrs  to retrieve from the search
   *
   * @throws  Exception  On test errors.
   */
  @Parameters({ "edid-query", "edid-returnAttrs" })
  @Test(groups = "edidtest")
  public void testSearch(final String query, final String returnAttrs)
    throws Exception
  {
    final SearchResponse searchResult = edid.search(query, returnAttrs.split("\\|"));

    LOGGER.debug("testSearch entries: {}", searchResult.getEntries());
    AssertJUnit.assertTrue(searchResult.entrySize() > 0);
  }

  /**
   * Test method for {@link EdId#getMembers(String)}.
   *
   * @param  uugid  to get members from
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-members-uugid")
  @Test(groups = "edidtest")
  public void testGetMembers(final String uugid)
    throws Exception
  {
    final String[] members = edid.getMembers(uugid);
    LOGGER.debug("testGetMembers members: {}", Arrays.asList(members));
    AssertJUnit.assertTrue(members.length > 0);
  }

  /**
   * Test method for {@link EdId#getGroupMembership(String)}.
   *
   * @param  uugid  to get membership from
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-groupmembership-uugid")
  @Test(groups = "edidtest")
  public void testGetGroupMembership(final String uugid)
    throws Exception
  {
    final String[] membership = edid.getGroupMembership(uugid);
    LOGGER.debug("testGetGroupMembership membership: {}", Arrays.asList(membership));
    AssertJUnit.assertTrue(membership.length > 0);
  }

  /**
   * Test method for {@link EdId#getMemberOf(String)}.
   *
   * @param  authId  to get members with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-memberof-authId")
  @Test(groups = "edidtest")
  public void testGetMemberOf(final String authId)
    throws Exception
  {
    final String[] memberOf = edid.getMemberOf(authId);
    LOGGER.debug("testGetMemberOf memberOf: {}", Arrays.asList(memberOf));
    AssertJUnit.assertTrue(memberOf.length > 0);
  }

  /**
   * Test method for {@link EdId#getVirginiaTechPerson(java.lang.String)}.
   *
   * @param  authId  to get VirginiaTechPerson with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-virginiatechperson-authId")
  @Test(groups = "edidtest")
  public void testGetVirginiaTechPerson(final String authId)
    throws Exception
  {
    final VirginiaTechPerson vtp = edid.getVirginiaTechPerson(authId);
    AssertJUnit.assertNotNull(vtp);
    AssertJUnit.assertNotNull(vtp.getUid());
  }

  /**
   * Test method for {@link EdId#getVirginiaTechGroup(java.lang.String)}.
   *
   * @param  uugidOrDn  to get VirginiaTechGroup with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-virginiatechgroup-uugid")
  @Test(groups = "edidtest")
  public void testGetVirginiaTechGroup(final String uugidOrDn)
    throws Exception
  {
    final VirginiaTechGroup vtp = edid.getVirginiaTechGroup(uugidOrDn);
    AssertJUnit.assertNotNull(vtp);
    AssertJUnit.assertNotNull(vtp.getUid());
    AssertJUnit.assertNotNull(vtp.getUugid());
  }

  /**
   * Test method for {@link EdId#getVirginiaTechEntitlementsByPid(java.lang.String)}.
   *
   * @param  authId  to get VirginiaTechEntitlement(s) with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-virginiatechentitlement-authId")
  @Test(groups = "edidtest")
  public void testGetVirginiaTechEntitlementsByPid(final String authId)
    throws Exception
  {
    final List<VirginiaTechEntitlement> entitlements = edid.getVirginiaTechEntitlementsByPid(authId);

    AssertJUnit.assertNotNull(entitlements);
    AssertJUnit.assertTrue(entitlements.size() > 0);

    for (VirginiaTechEntitlement entitlement : entitlements) {
      AssertJUnit.assertNotNull(entitlement.getUid());
    }
  }

  /**
   * Test method for {@link EdId#getVirginiaTechEntitlementsByViewerService(String)}.
   *
   * @param  uusid  to get VirginiaTechEntitlement(s) with
   *
   * @throws  Exception  On test errors.
   */
  @Parameters("edid-virginiatechentitlement-viewer")
  @Test(groups = "edidtest")
  public void testGetVirginiaTechEntitlementsByViewerService(final String uusid)
    throws Exception
  {
    final List<VirginiaTechEntitlement> entitlements = edid.getVirginiaTechEntitlementsByViewerService(uusid);

    AssertJUnit.assertNotNull(entitlements);
    AssertJUnit.assertTrue(entitlements.size() > 0);

    for (VirginiaTechEntitlement entitlement : entitlements) {
      AssertJUnit.assertNotNull(entitlement.getUid());
    }
  }
}
