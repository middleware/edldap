/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.catalina.realm;

import java.security.Principal;
import edu.vt.middleware.ldap.ed.DirectoryEnv;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EdAuthRealm}.
 *
 * @author  Middleware Services
 */
public class EdAuthRealmTest
{

  /** Auth ID used for all tests. */
  private String authId;

  /** Password for authId. */
  private String password;

  /** Subject of test. */
  private EdAuthRealm edAuthRealm;

  /**
   * @param  user  to auth with.
   * @param  pass  to auth with.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters(
    {
    "edauth-authId",
    "edauth-password"
  })
  @BeforeClass(groups = "edrealmtest")
  public void loadCredentials(final String user, final String pass)
  {
    authId = user;
    password = pass;
  }

  /**
   * @param  env  environment.
   *
   * @throws  Exception  On test failure.
   */
  @Parameters("environment")
  @BeforeClass(groups = "edrealmtest")
  public void createLdap(final DirectoryEnv env)
    throws Exception
  {
    edAuthRealm = new EdAuthRealm(env);
  }

  /**
   * Pos test method for {@link EdAuthRealm#authenticate(String, String)}
   */
  @Test(groups = "edrealmtest")
  public void authenticatePos()
  {
    final Principal principal = edAuthRealm.authenticate(authId, password);
    AssertJUnit.assertEquals(authId, principal.getName());
  }

  /**
   * Neg test method for {@link EdAuthRealm#authenticate(String, String)}
   */
  @Test(groups = "edrealmtest")
  public void authenticateNeg()
  {
    final Principal principal = edAuthRealm.authenticate(authId, "wrong-pass");
    AssertJUnit.assertNull(principal);
  }
}
