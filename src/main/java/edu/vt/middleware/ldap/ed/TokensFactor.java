/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.Credential;

/**
 * Represents a second factor to present to login.directory.vt.edu from ED Web Services /tokens endpoint. (see:
 * <a href="https://www.middleware.vt.edu/ed/ws/operations.html">Web Services Documentation</a>)
 * section: <b>Token Operations</b>
 *
 * @author Middleware Services
 */
public class TokensFactor implements MultiFactor
{

  /**
   * Default constructor.
   */
  public TokensFactor()
  {
  }

  @Override
  public Credential getValue(final Credential credentials)
  {
    return credentials;
  }

  @Override
  public MultiFactorType getType()
  {
    return MultiFactorType.TOKENS;
  }

  /**
   * Returns a {@link TokensFactor} instance.
   *
   * @return {@link TokensFactor} instance.
   */
  public static TokensFactor getInstance()
  {
    return new TokensFactor();
  }

}
