/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.Credential;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;

/**
 * Add facilities for authentication and authorization to ED clients.
 *
 * @author  Middleware Services
 */
public interface EdAuthService extends EdOperation
{


  /**
   * This will attempt to authenticate a user against ED-Auth. User should correspond to uupid.
   * Once this method has been called the connection to the LDAP is closed.
   *
   * @param  user  username for bind
   * @param  credential  credential for bind
   * @param  attributes  attributes to retrieve (ie {@link EdContext#ALL_ATTR}, {@link EdContext#AFFILIATION_ATTR}, ...)
   *
   * @return Found {@link LdapEntry}
   *
   * @throws  LdapException  if the authentication fails for any reason
   */
  LdapEntry authenticate(String user, Credential credential, String... attributes) throws LdapException;


  /**
   * This will attempt to authorize a user's given {@link LdapEntry} via the provided authorizationExpression.
   *
   * @param  entry  {@link LdapEntry} returned from authentication to authorize with
   * @param  authorizationExpression  Expression to authorize with
   *
   * @throws  LdapException  if the authentication fails for any reason
   * @throws  EdAuthAuthorizationException  if the authorization fails for any reason
   */
  void authorize(LdapEntry entry, String authorizationExpression) throws LdapException, EdAuthAuthorizationException;


  /**
   * This returns all the affiliations for the supplied entry.
   *
   * @param  authResult {@link LdapEntry} to parse
   *
   * @return  user's affiliations
   */
  String[] getAffiliations(LdapEntry authResult);


  /**
   * This returns all the group memberships for the supplied entry.
   *
   * @param  authResult {@link LdapEntry} to parse
   *
   * @return  user's group memberships
   */
  String[] getGroupMembership(LdapEntry authResult);


  /**
   * This will attempt to map {@link LdapEntry} to a {@link VirginiaTechPerson} bean.
   *
   * @param  authResult {@link LdapEntry} returned from authentication to parse
   *
   * @return  {@link VirginiaTechPerson} POJO representation of the {@link LdapEntry}.
   */
  VirginiaTechPerson getVirginiaTechPerson(LdapEntry authResult);
}
