/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.catalina.realm;


import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.naming.Context;
import javax.naming.NamingException;
import edu.vt.middleware.ldap.ed.EdAuthService;
import edu.vt.middleware.ldap.ed.EdContext;
import org.apache.catalina.Group;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Role;
import org.apache.catalina.Server;
import org.apache.catalina.User;
import org.apache.catalina.UserDatabase;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;
import org.ldaptive.Credential;
import org.ldaptive.DnParser;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class to provide drop in authentication against EdAuth using Catalina security realms.
 *
 * @author  Middleware Services
 */
public abstract class AbstractEdAuthRealm extends RealmBase
{

  /** Class logger instance. */
  protected final Logger logger = LoggerFactory.getLogger(getClass());

  /** Used to add additional roles to EdAuth users. */
  protected UserDatabase database;

  /** The global JNDI name of the EdAuth resource. */
  protected String resourceName;

  /** Directory class */
  protected EdAuthService auth;

  /**
   * Creates a new instance of an AbstractEdAuthRealm object that can provide authentication.
   *
   * @param  service  {@link EdAuthService}  to perform authentication
   * @param  name  {@link String} global JNDI name
   */
  public AbstractEdAuthRealm(final EdAuthService service, final String name)
  {
    auth = service;
    resourceName = name;
  }

  /**
   * This returns the global JNDI name of the {@link UserDatabase} resource used for adding additional roles to
   * authenticated users.
   *
   * @return  {@link String}
   */
  public String getResourceName()
  {
    return resourceName;
  }

  /**
   * This sets the global JNDI name of the {@link UserDatabase} resource used for adding additional roles to
   * authenticated users.
   *
   * @param  newResourceName  {@link String} new global JNDI name
   */
  public void setResourceName(final String newResourceName)
  {
    resourceName = newResourceName;
  }

  /**
   * This returns the {@link Principal} associated with the specified username and credentials, if there is one;
   * otherwise return null.
   *
   * @param  username  {@link String} to look up
   * @param  credentials  {@link String} to use in authenticating this username
   *
   * @return  {@link Principal}
   */
  @Override
  public Principal authenticate(final String username, final String credentials)
  {
    GenericPrincipal principal = null;

    try {
      final LdapEntry userEntry =
        auth.authenticate(username, new Credential(credentials), EdContext.AFFILIATION_ATTR,
          EdContext.GROUP_MEMBERSHIP_ATTR);

      // add EdAuth specific affiliations to roles
      final List<String> userRoles = new ArrayList<>();
      final String[] affil = auth.getAffiliations(userEntry);
      if (affil != null) {
        Collections.addAll(userRoles, affil);
      }

      // add EdAuth specific group membership to roles
      final String[] groupMembership = auth.getGroupMembership(userEntry);
      if (groupMembership != null) {
        Arrays.stream(groupMembership).forEach(
          groupMember -> userRoles.add(DnParser.getValue(groupMember, EdContext.UUGID_ATTR)));
      }

      // add any custom roles
      if (database != null) {
        final User user = database.findUser(username);
        if (user != null) {
          // lookup user's roles
          Iterator<Role> roles = user.getRoles();
          while (roles.hasNext()) {
            final Role role = roles.next();
            final String rolename = role.getRolename();
            if (!userRoles.contains(rolename)) {
              userRoles.add(rolename);
            }
          }

          final Iterator<Group> groups = user.getGroups();
          while (groups.hasNext()) {
            final Group group = groups.next();
            roles = group.getRoles();
            while (roles.hasNext()) {
              final Role role = roles.next();
              final String rolename = role.getRolename();
              if (!userRoles.contains(rolename)) {
                userRoles.add(rolename);
              }
            }
          }
        }
      }

      principal = new GenericPrincipal(username, null, userRoles);

    } catch (LdapException e) {
      logger.error("Could not authenticate user in realm.", e);
    }
    return principal;
  }

  /**
   * This method is not supported by this Realm. It will always return <b>null</b>.
   *
   * @param  username  {@link String}
   * @param  clientDigest  {@link String}
   * @param  nOnce  {@link String}
   * @param  nc  {@link String}
   * @param  cnonce  {@link String}
   * @param  qop  {@link String}
   * @param  realm  {@link String}
   * @param  md5a2  {@link String}
   *
   * @return  {@link Principal}
   */
  @Override
  public Principal authenticate(
    final String username,
    final String clientDigest,
    final String nOnce,
    final String nc,
    final String cnonce,
    final String qop,
    final String realm,
    final String md5a2)
  {
    return null;
  }

  /**
   * This method is not supported by this Realm. It will always return <b>null</b>.
   *
   * @param  certs  to authenticate
   *
   * @return  {@link Principal}
   */
  @Override
  public Principal authenticate(final X509Certificate[] certs)
  {
    return null;
  }

  /**
   * This method is not supported by this Realm. It will always return <b>null</b>.
   *
   * @param  username  {@link String}
   *
   * @return  {@link String}
   */
  @Override
  protected String getPassword(final String username)
  {
    return null;
  }

  /**
   * This returns the principal associated with the supplied user name.
   *
   * @param  username  {@link String}
   *
   * @return  {@link Principal}
   */
  @Override
  protected Principal getPrincipal(final String username)
  {
    if (database != null) {
      return database.findUser(username);
    } else {
      return null;
    }
  }

  /**
   * Prepare for active use of the public methods of this Component.
   *
   * @throws  LifecycleException  if this component detects a fatal error that prevents it from being started
   */
  @Override
  public synchronized void startInternal()
    throws LifecycleException
  {
    // attempt to load the EdAuth roles database
    // it's ok if the database cannot be loaded
    try {
      final MBeanServer mBeanServer = MBeanServerFactory.findMBeanServer(null).get(0);
      final ObjectName name = new ObjectName("Catalina", "type", "Server");
      final Server server = (Server) mBeanServer.getAttribute(name, "managedResource");

      final Context context = server.getGlobalNamingContext();
      database = (UserDatabase) context.lookup(resourceName);
    } catch (MalformedObjectNameException |
      MBeanException |
      AttributeNotFoundException |
      InstanceNotFoundException |
      ReflectionException |
      NamingException e) {
      database = null;
      logger.warn("Could not load custom roles database.", e);
    }
    super.startInternal();
  }

  /**
   * Gracefully shut down active use of the public methods of this Component.
   *
   * @throws  LifecycleException  if this component detects a fatal error that needs to be reported
   */
  @Override
  public synchronized void stopInternal()
    throws LifecycleException
  {
    super.stopInternal();
    database = null;
  }
}
