/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.Credential;

/**
 * Represents a second factor to present to login.directory.vt.edu
 * (see: <a href="https://www.middleware.vt.edu/ed/2fa-directory/index.html">Documentation</a>)
 *
 * <ul>
 * <li>AUTO</li>
 * <li>PHONE</li>
 * <li>SMS</li>
 * <li>PASSCODE</li>
 * <li>PUSH</li>
 * </ul>
 *
 * (Also see <a href="https://duo.com/docs/ldap">DUO documentation</a>.)
 *
 * @author Middleware Services
 */
public class DuoFactor implements MultiFactor
{

  /**
   * Enum defining the DUO second factor options.
   */
  public enum Type
  {
    /**
     * Automatic configuration
     */
    AUTO,
    /**
     * Phone callback
     */
    PHONE,
    /**
     * SMS (will fail, but you will get SMS codes to use for PASSCODE)
     */
    SMS,
    /**
     * Passcode callback (requires value ex: 123456)
     */
    PASSCODE,
    /**
     * App push callback
     */
    PUSH
  }

  /**
   * type
   */
  private final Type type;

  /**
   * factor index
   */
  private final int index;

  /**
   * factor value
   */
  private final String value;

  /**
   * Constructor which provides second factor type, and value. (ex: {@link Type#PASSCODE}, 066432)
   *
   * @param typeParameter {@link Type} of second-factor
   * @param valueParameter value of second-factor
   */
  public DuoFactor(final Type typeParameter, final String valueParameter)
  {
    this(typeParameter, 0, valueParameter);
  }

  /**
   * Constructor which provides second factor type, and index (ex: {@link Type#PHONE}, 2)
   * Useful for multiple factors registered.
   *
   * @param typeParameter {@link Type} of second-factor
   * @param indexParameter index of second-factor
   */
  public DuoFactor(final Type typeParameter, final int indexParameter)
  {
    this(typeParameter, indexParameter, null);
  }

  /**
   * Constructor which provides second factor type (ex: {@link Type#AUTO}, {@link Type#PHONE})
   *
   * @param typeParameter {@link Type} of second-factor
   */
  public DuoFactor(final Type typeParameter)
  {
    this(typeParameter, 0, null);
  }

  /**
   * Constructor which provides second factor type, index, and value.
   *
   * @param typeParameter {@link Type} of second-factor
   * @param indexParameter index of second-factor
   * @param valueParameter value of second-factor
   */
  private DuoFactor(final Type typeParameter, final int indexParameter, final String valueParameter)
  {
    this.type = typeParameter;
    this.index = indexParameter;
    this.value = valueParameter;
    validateInput(typeParameter, indexParameter, valueParameter);
  }

  @Override
  public Credential getValue(final Credential credential)
  {
    final StringBuilder sb = new StringBuilder(",");
    switch (type) {
    case PASSCODE:
      sb.append(value);
      break;
    default:
      sb.append(type.name().toLowerCase());
      if (index > 0) {
        sb.append(index);
      }
      break;
    }
    return new Credential(credential.getString().concat(sb.toString()));
  }

  @Override
  public MultiFactorType getType()
  {
    return MultiFactorType.DUO;
  }

  /**
   *
   * Returns a {@link DuoFactor} from {@link edu.vt.middleware.ldap.ed.executables.EdCli#MFA_VALUE_OPTION}
   * command-line arguments.
   *
   * @param mfaValue {@link edu.vt.middleware.ldap.ed.executables.EdCli#MFA_VALUE_OPTION} provided
   *
   * @return {@link DuoFactor}
   *
   * @throws IllegalArgumentException If {@link DuoFactor} cannot be instantiated
   */
  public static DuoFactor getInstance(final String mfaValue) throws IllegalArgumentException
  {
    if (mfaValue == null || mfaValue.isEmpty()) {
      throw new IllegalArgumentException("You MUST provide an mfaval for mfa DUO.  See --help");
    }
    final String[] mfaValueSplit = mfaValue.split(",");
    final DuoFactor.Type duoType;
    switch (mfaValueSplit.length) {
    case 1:
      duoType = DuoFactor.Type.valueOf(mfaValueSplit[0].trim().toUpperCase());
      return new DuoFactor(duoType);
    case 2:
      duoType = DuoFactor.Type.valueOf(mfaValueSplit[0].trim().toUpperCase());
      return duoType.equals(DuoFactor.Type.PASSCODE) ?
              new DuoFactor(duoType, mfaValueSplit[1]) :
              new DuoFactor(duoType, Integer.parseInt(mfaValueSplit[1]));
    default:
      throw new IllegalArgumentException("mfaval cannot be parsed for mfa DUO.  See --help");
    }
  }

  /**
   * Validates input after constructor.
   *
   * @param typeParameter type
   * @param indexParameter index
   * @param valueParameter value
   * @throws IllegalArgumentException If arguments are not valid
   */
  private void validateInput(final Type typeParameter,
          final int indexParameter, final String valueParameter) throws IllegalArgumentException
  {
    if (type == null) {
      throw new IllegalArgumentException("Type cannot be null");
    }
    if (typeParameter.equals(Type.AUTO) && (indexParameter > 0 || valueParameter != null)) {
      throw new IllegalArgumentException("Type.AUTO cannot have an index, nor a value");
    } else if (typeParameter.equals(Type.PASSCODE) && (valueParameter == null || indexParameter > 0)) {
      throw new IllegalArgumentException("Type.PASSCODE cannot have an index, and must have a value");
    } else if (typeParameter.equals(Type.SMS) && valueParameter != null) {
      throw new IllegalArgumentException("Type.SMS cannot have a value");
    } else if (typeParameter.equals(Type.PUSH) && valueParameter != null) {
      throw new IllegalArgumentException("Type.PUSH cannot have a value");
    } else if (typeParameter.equals(Type.PHONE) && valueParameter != null) {
      throw new IllegalArgumentException("Type.PHONE cannot have a value");
    }
  }

}
