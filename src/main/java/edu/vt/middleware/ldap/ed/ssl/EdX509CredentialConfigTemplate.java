/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.ssl;

import java.security.GeneralSecurityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * X509 Credential Configuration Template.
 *
 * @author  Middleware Services
 */
public class EdX509CredentialConfigTemplate extends EdX509CredentialConfig
{

  /** Class logger instance. */
  protected final Logger logger = LoggerFactory.getLogger(getClass());

  /**
   * Creates a new credential config.
   *
   * @param  authenticationCertificate  file or classpath location of the truststore
   * @param  authenticationKey  file or classpath location of the keystore
   * @param  trustCertificates  trust certificates
   */
  public EdX509CredentialConfigTemplate(
    final String authenticationCertificate,
    final String authenticationKey,
    final String trustCertificates)
  {

    setAuthenticationCertificate(authenticationCertificate);
    setAuthenticationKey(authenticationKey);
    setTrustCertificates(trustCertificates);

    try {
      createSSLContextInitializer();
    } catch (GeneralSecurityException e) {
      logger.debug("Could not initialize SSL context.", e);
    }
  }
}
