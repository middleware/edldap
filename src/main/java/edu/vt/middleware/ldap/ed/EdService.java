/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.util.Collection;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechGroup;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapException;
import org.ldaptive.SearchResponse;

/**
 * Interface that allows for performing common search operations on Virginia Tech Enterprise directory instances.
 *
 * @author  Middleware Services
 */
public interface EdService extends EdOperation
{


  /**
   * Searches an ED with the supplied query and return attributes. See {@link
   * org.ldaptive.AbstractOperation#execute(org.ldaptive.Request)}.
   *
   * @param  filter  expression to use for the search
   * @param  retAttrs  attributes to return
   *
   * @return  LDAP search results
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  SearchResponse search(String filter, String... retAttrs)
    throws LdapException;


  /**
   * Gets the requested attributes of the object whose distinguished name is given. See {@link
   * org.ldaptive.LdapEntry#getAttributes()}.
   *
   * @param  dn  LDAP distinguished name of object whose attributes will be fetched.
   * @param  retAttrs  attributes to return
   *
   * @return  ldap attributes
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  Collection<LdapAttribute> getAttributes(String dn, String... retAttrs)
    throws LdapException;


  /**
   * Gets the LDAP distinguished name for the supplied user.
   *
   * @param  authId  ED authId attribute value of user.
   *
   * @return  value of user's distinguished name.
   *
   * @throws  LdapException  if the LDAP search fails
   */
  String getDn(String authId)
    throws LdapException;


  /**
   * This will query LDAP for the specified authId for a VirginiaTechPerson entry and return the {@link
   * org.ldaptive.LdapEntry} mapped to a {@link VirginiaTechPerson} bean.
   *
   * @param  authId  ED authId attribute value of user.
   *
   * @return  VirginiaTechPerson POJO representation of Ldap result.
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  VirginiaTechPerson getVirginiaTechPerson(String authId)
    throws LdapException;


  /**
   * This will query LDAP for the specified uugidOrDn for a VirginiaTechGroup entry and return the {@link
   * org.ldaptive.LdapEntry} mapped to a {@link VirginiaTechGroup} bean.
   *
   * @param  uugidOrDn  Group to search for; may be either UUGID or DN.
   *
   * @return  VirginiaTechGroup POJO representation of Ldap result.
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  VirginiaTechGroup getVirginiaTechGroup(String uugidOrDn)
    throws LdapException;


  /**
   * Gets the groups of which the given group is a member.
   *
   * @param  uugidOrDn  Group to search for parents; may be either UUGID or DN.
   *
   * @return  Array of groups of which the given group is a member. Returns an empty array if the given group has member
   *          suppression enabled.
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  String[] getGroupMembership(String uugidOrDn)
    throws LdapException;


  /**
   * Gets the members of the given group.
   *
   * @param  uugidOrDn  UUGID or DN of group to search for members.
   *
   * @return  Array of group member names. Returns an empty array if the given group has member suppression enabled.
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  String[] getMembers(String uugidOrDn)
    throws LdapException;


  /**
   * Gets all the groups the supplied user is a member of.
   *
   * @param  authId  ED authId attribute value of user.
   *
   * @return  Array of group UUGID identifiers of which the given user is a member.
   *
   * @throws  LdapException  if the LDAP returns an error
   */
  String[] getMemberOf(String authId)
    throws LdapException;
}
