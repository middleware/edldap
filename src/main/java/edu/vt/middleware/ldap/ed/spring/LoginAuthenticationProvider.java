/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.spring;

import edu.vt.middleware.ldap.ed.Login;
import org.springframework.stereotype.Component;

/**
 * {@link LoginAuthenticationProvider} to provide drop in authentication provider for spring-security 3.2.x.
 *
 * @author  Middleware Services
 */
@Component
public class LoginAuthenticationProvider extends AbstractEdAuthAuthenticationProvider
{

  /**
   * Creates a new instance of an LoginAuthenticationProvider object that can provide authentication against Login for
   * the production environment.
   */
  public LoginAuthenticationProvider()
  {
    super(new Login());
  }
}
