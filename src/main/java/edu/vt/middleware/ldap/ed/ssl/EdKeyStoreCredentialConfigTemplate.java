/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.ssl;

import java.security.GeneralSecurityException;
import org.ldaptive.ssl.KeyStoreCredentialConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JKS Credential Configuration Template.
 *
 * @author  Middleware Services
 */
public class EdKeyStoreCredentialConfigTemplate extends KeyStoreCredentialConfig
{

  /** Class logger instance. */
  protected final Logger logger = LoggerFactory.getLogger(getClass());


  /**
   * Creates a new credential config.
   *
   * @param  truststore  file or classpath location of the truststore
   * @param  keystore  file or classpath location of the keystore
   */

  public EdKeyStoreCredentialConfigTemplate(final String truststore, final String keystore)
  {
    setTrustStore(truststore);
    setKeyStore(keystore);
    setKeyStorePassword("changeit");

    try {
      createSSLContextInitializer();
    } catch (GeneralSecurityException e) {
      logger.debug("Could not initialize socket factory, trying BKS keystore format", e);
      setKeyStoreType("BKS");
      try {
        createSSLContextInitializer();
      } catch (GeneralSecurityException e2) {
        logger.error("Could not initialize socket factory", e2);
      }
    }
  }
}
