/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.ssl;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import org.cryptacular.util.KeyPairUtil;
import org.ldaptive.ssl.AbstractCredentialReader;
import org.ldaptive.ssl.CredentialReader;
import org.ldaptive.ssl.SSLContextInitializer;
import org.ldaptive.ssl.X509CertificateCredentialReader;
import org.ldaptive.ssl.X509CertificatesCredentialReader;
import org.ldaptive.ssl.X509CredentialConfig;
import org.ldaptive.ssl.X509SSLContextInitializer;

/**
 * CredentialConfig implementation which uses cryptacular to support multiple private key formats.
 *
 * @author  Middleware Services
 */
public class EdX509CredentialConfig extends X509CredentialConfig
{

  /** Reads X.509 certificates credential. */
  private final X509CertificatesCredentialReader certsReader = new X509CertificatesCredentialReader();

  /** Reads X.509 certificate credential. */
  private final X509CertificateCredentialReader certReader = new X509CertificateCredentialReader();

  /** Reads private key credential. */
  private final CredentialReader<PrivateKey> keyReader = new AbstractCredentialReader<>() {

    /**
     * Reads a private key from an input stream.
     *
     * @param is     Input stream from which to read private key.
     * @param params Unused parameter.
     * @return Private key read from data in stream.
     * @throws IOException              On IO errors.
     * @throws GeneralSecurityException On errors with the credential data.
     */
    @Override
    public PrivateKey read(final InputStream is, final String... params)
      throws IOException, GeneralSecurityException
    {
      return KeyPairUtil.readPrivateKey(is);
    }
  };


  @Override
  public SSLContextInitializer createSSLContextInitializer()
    throws GeneralSecurityException
  {
    final X509SSLContextInitializer sslInit = new X509SSLContextInitializer();
    try {
      if (getTrustCertificates() != null) {
        sslInit.setTrustCertificates(certsReader.read(getTrustCertificates()));
      }
      if (getAuthenticationCertificate() != null) {
        sslInit.setAuthenticationCertificate(certReader.read(getAuthenticationCertificate()));
      }
      if (getAuthenticationKey() != null) {
        sslInit.setAuthenticationKey(keyReader.read(getAuthenticationKey()));
      }
    } catch (IOException e) {
      throw new GeneralSecurityException(e);
    }
    return sslInit;
  }
}
