/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

/**
 * Represents a deployment environment for a particular class of the Enterprise Directory. All directory classes have
 * the following environments:
 *
 * <ul>
 *   <li>Local - For testing docker image.</li>
 *   <li>Development - For rough testing.</li>
 *   <li>Pre-production - A staging environment to test new, refined versions on their way to production.</li>
 *   <li>Production - The live, production environment.</li>
 * </ul>
 *
 * @author  Middleware Services
 */
public enum DirectoryEnv {

  /** Local integration test environment */
  LOCAL,

  /** Development environment. */
  DEV,

  /** Pre-prod environment. */
  PPRD,

  /** Production environment. */
  PROD
}
