/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

/**
 * EdAuth provides methods to perform common authentication and authorization operations.
 *
 * @author  Middleware Services
 */
public final class EdAuth extends AbstractEdAuthService
{


  /**
   * Creates a new instance of an ED client object that can perform operations on the <em>production</em> ED-Auth
   * directory.
   */
  public EdAuth()
  {
    this(EdContext.getDefaultEnvironment());
  }


  /**
   * Creates a new instance of an ED client object that can perform operations on an ED-Auth directory of the given
   * environment.
   *
   * @param  env  Directory environment, e.g. LOCAL|DEV|PPRD|PROD.
   */
  public EdAuth(final DirectoryEnv env)
  {
    setEnvironment(env);
    initialize();
  }


  @Override
  public DirectoryType getType()
  {
    return DirectoryType.EDAUTH;
  }
}
