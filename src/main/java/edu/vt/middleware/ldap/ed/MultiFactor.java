/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.Credential;

/**
 * Represents a multi-factor authentication data to present to an ldap instance.
 *
 * @author Middleware Services
 */
public interface MultiFactor
{

  /**
   * Returns the processed {@link Credential} by this factor.
   *
   * @param credential {@link Credential} to derive from
   *
   * @return Generated value
   */
  Credential getValue(Credential credential);

  /**
   * Returns the type associated with this multi-factor instance.
   *
   * @return {@link MultiFactorType}
   */
  MultiFactorType getType();

  /**
   * Returns an instance of known sub-types (see: {@link MultiFactorType}).
   * This is a convenience method which returns the appropriate implementation with the specified type.
   *
   * @param type {@link MultiFactorType} type of {@link MultiFactor}
   * @param mfaVal {@link edu.vt.middleware.ldap.ed.executables.EdCli#MFA_VALUE_OPTION}
   * @return Appropriate instance with the type, will throw {@link IllegalArgumentException} if none matches
   */
  static MultiFactor getInstance(final MultiFactorType type, final String mfaVal)
  {
    switch (type) {
    case DUO:
      return DuoFactor.getInstance(mfaVal);
    case TOKENS:
      return TokensFactor.getInstance();
    default:
      throw new IllegalArgumentException("unknown MultiFactorType");
    }
  }
}
