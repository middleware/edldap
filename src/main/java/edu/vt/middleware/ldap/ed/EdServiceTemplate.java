/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechGroup;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.ConnectionFactory;
import org.ldaptive.DnParser;
import org.ldaptive.FilterTemplate;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.PooledConnectionFactory;
import org.ldaptive.SearchOperation;
import org.ldaptive.SearchRequest;
import org.ldaptive.SearchResponse;
import org.ldaptive.beans.reflect.DefaultLdapEntryMapper;

/**
 * Contains functions used by instances of ED which are intended for searching.
 *
 * @author  Middleware Services
 */
public class EdServiceTemplate implements EdService
{

  /** Ldap entry MAPPER for object conversion. */
  protected static final DefaultLdapEntryMapper<Object> MAPPER = new DefaultLdapEntryMapper<>();

  /** Underlying connection pool for ldap. */
  private PooledConnectionFactory baseConnectionFactory;

  /** Directory environment. */
  private DirectoryEnv environment;

  /** Directory type. */
  private DirectoryType type;


  /**
   * Default constructor. {@link EdServiceTemplate#setType(DirectoryType)} and {@link
   * EdServiceTemplate#setEnvironment(DirectoryEnv)} must be called to configure this search client. {@link
   * #initialize()} must be called before this object can be used.
   */
  public EdServiceTemplate() {}


  /**
   * This creates a new instance of an ED search client. {@link #initialize()} must be called before this object can be
   * used.
   *
   * @param  directoryType  directory type
   * @param  env  directory env
   */
  public EdServiceTemplate(final DirectoryType directoryType, final DirectoryEnv env)
  {
    setType(directoryType);
    setEnvironment(env);
  }


  @Override
  public DirectoryType getType()
  {
    return type;
  }


  /**
   * Sets the directory type this ED client is configured to operate on.
   *
   * @param  dirType  Directory class type.
   */
  protected void setType(final DirectoryType dirType)
  {
    type = dirType;
  }


  @Override
  public DirectoryEnv getEnvironment()
  {
    return environment;
  }


  /**
   * Sets the directory environment this ED client is configured to operate on.
   *
   * @param  dirEnv  Directory environment.
   */
  protected void setEnvironment(final DirectoryEnv dirEnv)
  {
    environment = dirEnv;
  }


  /**
   * Initialize this search client for use.
   *
   * <p>This method <strong>must</strong> be called after the directory type ({@link #setType(DirectoryType)}) and
   * environment ({@link #setEnvironment(DirectoryEnv)}) are set and before any LDAP operations are performed. If the
   * directory type and/or environment are changed, the search client must be re-initialized for the changes to take
   * effect on the next LDAP operation.</p>
   */
  public void initialize()
  {
    baseConnectionFactory = new PooledConnectionFactory(EdContext.createConnectionConfig(type, environment));
    baseConnectionFactory.initialize();
  }


  /**
   * Close the connection pool used by the connection factory.
   */
  public void close()
  {
    baseConnectionFactory.close();
  }


  @Override
  public SearchResponse search(final String filter, final String... retAttrs)
    throws LdapException
  {
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    return search.execute(new SearchRequest(EdContext.BASE_DN, new FilterTemplate(filter), retAttrs));
  }


  @Override
  public Collection<LdapAttribute> getAttributes(final String dn, final String... retAttrs)
    throws LdapException
  {
    final Collection<LdapAttribute> returnAttributes = new ArrayList<>();
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      returnAttributes.addAll(e.getAttributes());
      return e;
    });
    search.execute(SearchRequest.objectScopeSearchRequest(dn, retAttrs));
    return returnAttributes;
  }


  @Override
  public String getDn(final String authId)
    throws LdapException
  {
    if (authId == null || authId.length() == 0) {
      throw new IllegalArgumentException("authId cannot be null or empty.");
    }

    String dn = null;
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.USER_FILTER);
    searchFilter.setParameter("user", authId);
    final SearchResponse result = search.execute(new SearchRequest(EdContext.PEOPLE_BASE_DN, searchFilter));
    if (result.getEntry() != null) {
      dn = result.getEntry().getDn();
    }
    return dn;
  }


  @Override
  public String[] getGroupMembership(final String uugidOrDn)
    throws LdapException
  {
    if (uugidOrDn == null || uugidOrDn.length() == 0) {
      throw new IllegalArgumentException("uugidOrDn cannot be null or empty.");
    }

    final List<String> memberships = new ArrayList<>();
    final List<LdapEntry> answer = new ArrayList<>();
    final String searchUugid = toGroupUugid(uugidOrDn);

    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      answer.add(e);
      return e;
    });
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.GROUP_FILTER);
    searchFilter.setParameter("group", searchUugid);
    final SearchRequest searchRequest = new SearchRequest(
      EdContext.GROUPS_BASE_DN,
      searchFilter,
      EdContext.GROUP_MEMBERSHIP_ATTR);
    search.execute(searchRequest);

    answer.stream()
      .filter(Objects::nonNull)
      .map(ldapEntry -> ldapEntry.getAttribute(EdContext.GROUP_MEMBERSHIP_ATTR))
      .filter(Objects::nonNull).forEach(attr -> memberships.addAll(attr.getStringValues()));
    return memberships.toArray(new String[0]);
  }


  @Override
  public String[] getMembers(final String uugidOrDn)
    throws LdapException
  {
    if (uugidOrDn == null || uugidOrDn.length() == 0) {
      throw new IllegalArgumentException("uugidOrDn cannot be null or empty.");
    }

    final List<String> members = new ArrayList<>();
    final String searchUugid = toGroupUugid(uugidOrDn);

    final List<LdapEntry> answer = new ArrayList<>();
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      answer.add(e);
      return e;
    });
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.GROUP_FILTER);
    searchFilter.setParameter("group", searchUugid);
    final SearchRequest searchRequest = new SearchRequest(
      EdContext.GROUPS_BASE_DN,
      searchFilter,
      EdContext.MEMBER_ATTR);
    search.execute(searchRequest);
    answer.stream()
      .filter(Objects::nonNull)
      .map(ldapEntry -> ldapEntry.getAttribute(EdContext.MEMBER_ATTR))
      .filter(Objects::nonNull)
      .forEach(ldapAttribute -> members.addAll(ldapAttribute.getStringValues()));
    return members.toArray(new String[0]);
  }


  @Override
  public String[] getMemberOf(final String authId)
    throws LdapException
  {
    final String dn = getDn(authId);
    final List<String> members = new ArrayList<>();
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      members.add(e.getDn());
      return e;
    });
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.MEMBER_FILTER);
    searchFilter.setParameter("group", dn);

    final SearchRequest searchRequest = new SearchRequest(
      EdContext.GROUPS_BASE_DN,
      searchFilter,
      EdContext.GROUP_MEMBERSHIP_ATTR);
    search.execute(searchRequest);
    return members.toArray(new String[0]);
  }


  /**
   * Converts a group UUGID or DN to a group UUGID. Since the method makes the determination of the type of input, if
   * given a UUGID it simply returns it.
   *
   * @param  uugidOrDn  Group UUGID or DN.
   *
   * @return  Group UUGID.
   */
  protected static String toGroupUugid(final String uugidOrDn)
  {
    if (uugidOrDn.endsWith(EdContext.GROUPS_BASE_DN)) {
      return DnParser.getValue(uugidOrDn, EdContext.UUGID_ATTR);
    } else {
      return uugidOrDn;
    }
  }


  /**
   * Converts a uusid to include the base DN.
   *
   * @param  uusidOrDn  Service UUSID or DN.
   *
   * @return  uusid with the base DN, if uusidOrDn already includes the base DN then it simply returns it.
   */
  protected static String toUusidDn(final String uusidOrDn)
  {
    String dn;
    final StringBuilder sb = new StringBuilder(EdContext.UUSID_ATTR).append("=");
    try {
      dn = DnParser.getValue(uusidOrDn, EdContext.UUSID_ATTR);
    } catch (Exception e) {
      dn = uusidOrDn;
    }
    sb.append(dn).append(",").append(EdContext.SERVICES_BASE_DN);
    return sb.toString();
  }


  @Override
  public VirginiaTechPerson getVirginiaTechPerson(final String authId)
    throws LdapException
  {

    if (authId == null || authId.length() == 0) {
      throw new IllegalArgumentException("authId cannot be null or empty.");
    }

    final VirginiaTechPerson vtPerson = new VirginiaTechPerson();
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.USER_FILTER);
    searchFilter.setParameter("user", authId);

    final SearchResponse result = search.execute(new SearchRequest(EdContext.PEOPLE_BASE_DN, searchFilter));
    if (result.getEntry() != null) {
      MAPPER.map(result.getEntry(), vtPerson);
    }
    return vtPerson;
  }


  @Override
  public VirginiaTechGroup getVirginiaTechGroup(final String uugidOrDn)
    throws LdapException
  {

    if (uugidOrDn == null || uugidOrDn.length() == 0) {
      throw new IllegalArgumentException("uugidOrDn cannot be null or empty.");
    }

    final String searchUugid = toGroupUugid(uugidOrDn);
    final VirginiaTechGroup vtGroup = new VirginiaTechGroup();

    final SearchOperation search = new SearchOperation(getConnectionFactory());
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.GROUP_FILTER);
    searchFilter.setParameter("group", searchUugid);

    final SearchResponse result = search.execute(new SearchRequest(EdContext.GROUPS_BASE_DN, searchFilter));
    if (result.getEntry() != null) {
      MAPPER.map(result.getEntry(), vtGroup);
    }
    return vtGroup;
  }

  @Override
  public ConnectionFactory getConnectionFactory()
  {
    return baseConnectionFactory;
  }
}
