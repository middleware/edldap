/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.jetty.realm;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.security.auth.Subject;
import javax.servlet.ServletRequest;
import edu.vt.middleware.ldap.ed.EdAuthService;
import edu.vt.middleware.ldap.ed.EdContext;
import org.eclipse.jetty.security.DefaultIdentityService;
import org.eclipse.jetty.security.IdentityService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.ldaptive.Credential;
import org.ldaptive.DnParser;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.jaas.LdapPrincipal;
import org.ldaptive.jaas.LdapRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class to provide drop in authentication against EdAuth using Jetty security realms.
 *
 * @author  Middleware Services
 */
public abstract class AbstractEdAuthRealm extends AbstractLifeCycle implements LoginService
{

  /** Class logger instance. */
  protected final Logger logger = LoggerFactory.getLogger(getClass());

  /** Default IdentityService for realm. */
  protected final IdentityService identityService = new DefaultIdentityService();

  /** Descriptive information about this Realm implementation. */
  protected String realmName;

  /** Directory class */
  protected EdAuthService auth;

  /**
   * Creates a new instance of an EDAuthRealm object that can provide authentication against EdAuth for the production
   * environment.
   *
   * @param  service  {@link EdAuthService}  to perform authentication
   * @param  name  {@link String} realm name
   */
  public AbstractEdAuthRealm(final EdAuthService service, final String name)
  {
    auth = service;
    realmName = name;
  }

  /**
   * Returns the name of the realm. (default: EdAuthRealm)
   *
   * @return  {@link String}
   */
  @Override
  public String getName()
  {
    return realmName;
  }

  /**
   * Sets the name of the realm.
   *
   * @param  name  {@link String}
   */
  public void setName(final String name)
  {
    realmName = name;
  }

  /**
   * The core function which authenticates the user and gathers the roles.
   *
   * @param  username  {@link String}
   * @param  credentials  {@link Object}
   * @param  servletRequest {@link ServletRequest}
   *
   * @return  {@link UserIdentity} if the user is authenticated. returns <b>null</b> otherwise.
   */
  @Override
  public UserIdentity login(final String username, final Object credentials, final ServletRequest servletRequest)
  {
    UserIdentity user = null;
    try {
      final LdapEntry userEntry =
        auth.authenticate(username, new Credential(credentials.toString()), EdContext.AFFILIATION_ATTR,
          EdContext.GROUP_MEMBERSHIP_ATTR);
      // add EdAuth specific affiliations to roles
      final List<String> userRoles = new ArrayList<>();
      final String[] affil = auth.getAffiliations(userEntry);
      if (affil != null) {
        Collections.addAll(userRoles, affil);
      }
      // add EdAuth specific group membership to roles
      final String[] groupMembership = auth.getGroupMembership(userEntry);
      if (groupMembership != null) {
        Arrays.stream(groupMembership).forEach(
          groupMember -> userRoles.add(DnParser.getValue(groupMember, EdContext.UUGID_ATTR)));
      }
      final Principal authenticatedPrincipal = new LdapPrincipal(username, userEntry);
      final Subject subject = new Subject();
      subject.getPrincipals().add(authenticatedPrincipal);

      userRoles.forEach(role -> subject.getPrincipals().add(new LdapRole(role)));
      subject.setReadOnly();
      user = identityService.newUserIdentity(
        subject,
        authenticatedPrincipal,
        userRoles.toArray(new String[0]));
    } catch (LdapException e) {
      logger.error("Could not authenticate user in realm.", e);
    }
    return user;
  }

  /**
   * This method will return true is the user is not null, false otherwise.
   *
   * @param  user  {@link UserIdentity}
   *
   * @return  {@link boolean}
   */
  @Override
  public boolean validate(final UserIdentity user)
  {
    return user != null;
  }

  /**
   * This method is not supported by this Realm. It will do nothing.
   *
   * @param  user  {@link UserIdentity}
   */
  @Override
  public void logout(final UserIdentity user)
  {
    // Do nothing.
  }

  /**
   * Return the default {@link IdentityService}
   *
   * @return  {@link IdentityService}
   */
  @Override
  public IdentityService getIdentityService()
  {
    return identityService;
  }

  /**
   * This method is not supported in this realm.
   *
   * @param  iService  {@link IdentityService}
   */
  @Override
  public void setIdentityService(final IdentityService iService)
  {
    // Do nothing.
  }
}
