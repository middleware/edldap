/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.catalina.realm;

import edu.vt.middleware.ldap.ed.DirectoryEnv;
import edu.vt.middleware.ldap.ed.EdAuth;

/**
 * {@link EdAuthRealm} to provide drop in authentication against EdAuth using Catalina security realms.
 *
 * @author  Middleware Services
 */
public class EdAuthRealm extends AbstractEdAuthRealm
{

  /**
   * Creates a new instance of an EDAuthRealm object that can provide authentication against EdAuth for the production
   * environment.
   */
  public EdAuthRealm()
  {
    super(new EdAuth(), "EdAuthDatabase");
  }

  /**
   * Creates a new instance of an EDAuthRealm object that can provide authentication against EdAuth for the given
   * environment.
   *
   * @param  env  Directory environment, e.g. LOCAL|DEV|PPRD|PROD.
   */
  public EdAuthRealm(final DirectoryEnv env)
  {
    super(new EdAuth(env), "EdAuthDatabase");
  }
}
