/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.util.ArrayList;
import java.util.List;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechEntitlement;
import org.ldaptive.FilterTemplate;
import org.ldaptive.LdapException;
import org.ldaptive.SearchOperation;
import org.ldaptive.SearchRequest;

/**
 * EdId provides for the common ED client operations acting on the ED-ID directory.
 *
 * @author  Middleware Services
 */
public final class EdId extends EdServiceTemplate
{

  /**
   * Creates a new instance of an ED client object that can perform operations on the <em>production</em> ED-ID
   * directory.
   */
  public EdId()
  {
    this(EdContext.getDefaultEnvironment());
  }

  /**
   * Creates a new instance of an ED client object that can perform operations on an ED-ID directory of the given
   * environment.
   *
   * @param  env  directory env
   */
  public EdId(final DirectoryEnv env)
  {
    super(DirectoryType.EDID, env);
    initialize();
  }

  /**
   * Gets a List of VirginiaTechEntitlement objects from a given viewer uusid.
   *
   * @param  uusidOrDn  uusidOrDn can be the name of the viewer service directly or its full DN.
   *
   * @return  {@link List}&lt;{@link VirginiaTechEntitlement}&gt; objects
   *
   * @throws  LdapException  For any LDAP errors.
   */
  public List<VirginiaTechEntitlement> getVirginiaTechEntitlementsByViewerService(final String uusidOrDn)
    throws LdapException
  {
    if (uusidOrDn == null || uusidOrDn.length() == 0) {
      throw new IllegalArgumentException("uusidOrDn cannot be null or empty.");
    }
    final List<VirginiaTechEntitlement> entitlements = new ArrayList<>();
    final String uusidDn = toUusidDn(uusidOrDn);
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      final VirginiaTechEntitlement ent = new VirginiaTechEntitlement();
      MAPPER.map(e, ent);
      entitlements.add(ent);
      return e;
    });
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.ENTITLEMENT_VIEWER_FILTER);
    searchFilter.setParameter("entitlement", uusidDn);
    search.execute(new SearchRequest(EdContext.ENTITLEMENTS_BASE_DN, searchFilter));
    return entitlements;
  }

  /**
   * Gets a List of VirginiaTechEntitlement objects from a given authId.
   *
   * @param  authId  authId of the person whose entitlements will be retrieved.
   *
   * @return  {@link List}&lt;{@link VirginiaTechEntitlement}&gt; objects
   *
   * @throws  LdapException  For any LDAP errors.
   */
  public List<VirginiaTechEntitlement> getVirginiaTechEntitlementsByPid(final String authId)
    throws LdapException
  {

    if (authId == null || authId.length() == 0) {
      throw new IllegalArgumentException("authId cannot be null or empty.");
    }

    final List<VirginiaTechEntitlement> entitlements = new ArrayList<>();
    final String userDn = getDn(authId);
    final SearchOperation search = new SearchOperation(getConnectionFactory());
    search.setEntryHandlers(e -> {
      final VirginiaTechEntitlement ent = new VirginiaTechEntitlement();
      MAPPER.map(e, ent);
      entitlements.add(ent);
      return e;
    });
    final FilterTemplate searchFilter = new FilterTemplate(EdContext.ENTITLEMENT_FILTER);
    searchFilter.setParameter("entitlement", userDn);
    search.execute(new SearchRequest(EdContext.ENTITLEMENTS_BASE_DN, searchFilter));
    return entitlements;
  }
}
