/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.spring;

import edu.vt.middleware.ldap.ed.EdAuth;
import org.springframework.stereotype.Component;

/**
 * {@link EdAuthAuthenticationProvider} to provide drop in authentication provider for spring-security 3.2.x.
 *
 * @author  Middleware Services
 */
@Component
public class EdAuthAuthenticationProvider extends AbstractEdAuthAuthenticationProvider
{

  /**
   * Creates a new instance of an EdAuthAuthenticationProvider object that can provide authentication against Login for
   * the production environment.
   */
  public EdAuthAuthenticationProvider()
  {
    super(new EdAuth());
  }
}
