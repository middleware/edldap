/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

/**
 * EdLite provides for the common ED client operations acting on the ED-Lite directory.
 *
 * @author  Middleware Services
 */
public final class EdLite extends EdServiceTemplate
{


  /**
   * Creates a new instance of an ED client object that can perform operations on the <em>production</em> ED-Lite
   * directory.
   */
  public EdLite()
  {
    this(EdContext.getDefaultEnvironment());
  }


  /**
   * Creates a new instance of an ED client object that can perform operations on an ED-Lite directory of the given
   * environment.
   *
   * @param  env  directory env
   */
  public EdLite(final DirectoryEnv env)
  {
    super(DirectoryType.EDLITE, env);
    initialize();
  }
}
