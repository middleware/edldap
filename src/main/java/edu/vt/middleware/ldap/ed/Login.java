/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.Credential;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;

/**
 * Provides methods to perform common authentication and authorization operations using the 2FA directory.  As per
 * documentation, if no {@link MultiFactor} is provided auto configuration will be used.<br>
 * See <a href="https://www.middleware.vt.edu/ed/2fa-directory/index.html">documentation</a>.
 *
 * @author  Middleware Services
 */
public final class Login extends AbstractEdAuthService
{


  /**
   * Creates a new instance of an ED client object that can perform operations on the <em>production</em> login
   * directory.
   */
  public Login()
  {
    this(EdContext.getDefaultEnvironment());
  }


  /**
   * Creates a new instance of an ED client object that can perform operations on login directory of the given
   * environment.
   *
   * @param  env  Directory environment, e.g. LOCAL|DEV|PPRD|PROD.
   */
  public Login(final DirectoryEnv env)
  {
    setEnvironment(env);
    initialize();
  }


  @Override
  public DirectoryType getType()
  {
    return DirectoryType.LOGIN;
  }


  /**
   * Same as {@link EdAuth#authenticate(String, Credential, String...)} with a second factor.
   *
   * @param  user  username for bind
   * @param  credential  credential for bind
   * @param  secondFactor second factor for bind (see {@link MultiFactor})
   * @param  attributes  attributes to retrieve (ie {@link EdContext#ALL_ATTR}, {@link EdContext#AFFILIATION_ATTR}, ...)
   *
   * @return Found {@link LdapEntry}
   *
   * @throws  LdapException  if the authentication fails for any reason
   *
   * @see EdAuth#authenticate(String, Credential, String...)
   */
  public LdapEntry authenticate(
    final String user, final Credential credential, final MultiFactor secondFactor, final String... attributes)
    throws LdapException
  {
    return authenticate(user, secondFactor.getValue(credential), attributes);
  }
}
