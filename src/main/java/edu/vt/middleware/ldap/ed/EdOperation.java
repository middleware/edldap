/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import org.ldaptive.ConnectionFactory;

/**
 * Interface that allows for performing common operations on Virginia Tech Enterprise directory instances.
 *
 * @author  Middleware Services
 */
public interface EdOperation
{

  /**
   * Gets the underlying connection factory.
   *
   * @return  Connection factory implementation.
   */
  ConnectionFactory getConnectionFactory();

  /**
   * Gets the class of directory this client operates on.
   *
   * @return  Directory type.
   */
  DirectoryType getType();


  /**
   * Gets the environment this client operates on.
   *
   * @return  Directory environment.
   */
  DirectoryEnv getEnvironment();
}
