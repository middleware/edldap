/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import edu.vt.middleware.ldap.ed.ssl.EdKeyStoreCredentialConfigTemplate;
import edu.vt.middleware.ldap.ed.ssl.EdX509CredentialConfigTemplate;
import org.ldaptive.BindConnectionInitializer;
import org.ldaptive.ConnectionConfig;
import org.ldaptive.sasl.Mechanism;
import org.ldaptive.sasl.SaslConfig;
import org.ldaptive.ssl.AllowAnyHostnameVerifier;
import org.ldaptive.ssl.SslConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates ed client objects to interact with various ED directories.
 *
 * @author  Middleware Services
 */
public final class EdContext
{

  /** Base DN for Enterprise Directory operations. */
  public static final String BASE_DN = "dc=vt,dc=edu";

  /** People branch base DN for Enterprise Directory operations. */
  public static final String PEOPLE_BASE_DN = "ou=People,dc=vt,dc=edu";

  /** Groups branch base DN for Enterprise Directory operations. */
  public static final String GROUPS_BASE_DN = "ou=Groups,dc=vt,dc=edu";

  /** Entitlements branch base DN for Enterprise Directory operations. */
  public static final String ENTITLEMENTS_BASE_DN = "ou=Entitlements,dc=vt,dc=edu";

  /** Services branch base DN for Enterprise Directory operations. */
  public static final String SERVICES_BASE_DN = "ou=Services,dc=vt,dc=edu";

  /** Entitlement filter for finding a specific entitlement object. */
  public static final String ENTITLEMENT_FILTER = "(entitled={entitlement})";

  /** Entitlement filter for finding a specific entitlement object by its service viewer. */
  public static final String ENTITLEMENT_VIEWER_FILTER = "(viewer={entitlement})";

  /** User identifier attribute for Enterprise Directory operations. */
  public static final String USERID_ATTR = "authId";

  /** User filter for finding a person. */
  public static final String USER_FILTER = "(authId={user})";

  /** Group identifier attribute for Enterprise Directory operations. */
  public static final String UUGID_ATTR = "uugid";

  /** Service identifier attribute for Enterprise Directory operations. */
  public static final String UUSID_ATTR = "uusid";

  /** Group filter for finding a group. */
  public static final String GROUP_FILTER = "(uugid={group})";

  /** Affiliation attribute for Enterprise Directory operations. */
  public static final String AFFILIATION_ATTR = "eduPersonAffiliation";

  /** Group membership attribute for Enterprise Directory group operations. */
  public static final String GROUP_MEMBERSHIP_ATTR = "groupMembership";

  /** All attributes available for Enterprise Directory operations. */
  public static final String ALL_ATTR = "*";

  /** Member attribute for Enterprise Directory group operations. */
  public static final String MEMBER_ATTR = "member";

  /** Member filter for finding a group. */
  public static final String MEMBER_FILTER = "(member={group})";

  /** Maps ED directory TYPE-ENV to host names. */
  private static final Map<String, String> HOST_MAP = new HashMap<>();

  /** Maps ED directory TYPE-ENV to host names. */
  private static final Map<String, String> DEFAULT_ED_PROPERTIES = new HashMap<>();

  /** All configuration related to edldap. */
  private static final Properties ED_PROPERTIES = new Properties();

  /** properties filename. */
  private static final String ED_PROPERTIES_FILENAME = "edldap.properties";

  static {
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDAUTH, DirectoryEnv.LOCAL), "ldap://localhost");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDAUTH, DirectoryEnv.DEV), "ldap://ed-dev.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDAUTH, DirectoryEnv.PPRD), "ldap://ed-pprd.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDAUTH, DirectoryEnv.PROD), "ldap://authn.directory.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDID, DirectoryEnv.LOCAL), "ldap://localhost");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDID, DirectoryEnv.DEV), "ldap://ed-dev.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDID, DirectoryEnv.PPRD), "ldap://ed-pprd.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDID, DirectoryEnv.PROD), "ldap://id.directory.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDLITE, DirectoryEnv.LOCAL), "ldap://localhost");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDLITE, DirectoryEnv.DEV), "ldap://ed-dev.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDLITE, DirectoryEnv.PPRD), "ldap://ed-pprd.middleware.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.EDLITE, DirectoryEnv.PROD), "ldap://directory.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.LOGIN, DirectoryEnv.LOCAL), "ldap://localhost");
    HOST_MAP.put(getTypeEnvName(DirectoryType.LOGIN, DirectoryEnv.DEV), "ldap://login-dev.directory.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.LOGIN, DirectoryEnv.PPRD), "ldap://login-pprd.directory.vt.edu");
    HOST_MAP.put(getTypeEnvName(DirectoryType.LOGIN, DirectoryEnv.PROD), "ldap://login.directory.vt.edu");

    DEFAULT_ED_PROPERTIES.put("edldap.edauth.ssl.type", "JKS");
    DEFAULT_ED_PROPERTIES.put(
      "edldap.edauth.ssl.truststorePath",
      "classpath:/edu/vt/middleware/ldap/ed/ssl/ed.truststore");
    DEFAULT_ED_PROPERTIES.put(
      "edldap.edauth.ssl.trustCertificatesPath",
      "classpath:/edu/vt/middleware/ldap/ed/ssl/trustchain.pem");
    DEFAULT_ED_PROPERTIES.put("edldap.edid.ssl.type", "JKS");
    DEFAULT_ED_PROPERTIES.put("edldap.edid.ssl.keystorePath", "classpath:/edid.keystore");
    DEFAULT_ED_PROPERTIES.put(
      "edldap.edid.ssl.truststorePath",
      "classpath:/edu/vt/middleware/ldap/ed/ssl/ed.truststore");
    DEFAULT_ED_PROPERTIES.put("edldap.edid.ssl.authenticationCertificatePath", "classpath:/edid.pem");
    DEFAULT_ED_PROPERTIES.put("edldap.edid.ssl.authenticationCertificateKeyPath", "classpath:/edid.key");
    DEFAULT_ED_PROPERTIES.put(
      "edldap.edid.ssl.trustCertificatesPath",
      "classpath:/edu/vt/middleware/ldap/ed/ssl/trustchain.pem");

    final String edldapPropertiesPath = System.getProperty("edldap.properties");
    if (edldapPropertiesPath != null) {
      try (InputStream fis = new FileInputStream(edldapPropertiesPath)) {
        ED_PROPERTIES.load(fis);
      } catch (IOException e) {
        throw new IllegalArgumentException("Could not load properties from: " + edldapPropertiesPath, e);
      }
    } else {
      final Logger logger = LoggerFactory.getLogger(EdContext.class);
      logger.debug("Did not find system property 'edldap.properties'.");
      try (InputStream is = getClassLoader().getResourceAsStream(ED_PROPERTIES_FILENAME)) {
        if (is != null) {
          ED_PROPERTIES.load(is);
        }
      } catch (IOException e) {
        throw new IllegalStateException("Could not load " + ED_PROPERTIES_FILENAME + " from the classpath", e);
      }
    }
  }

  /** Constructor of static factory class. */
  private EdContext() {}

  /**
   * Return the thread context ClassLoader otherwise fallback to EdContext class loader.
   *
   * @return  ClassLoader
   */
  private static ClassLoader getClassLoader()
  {
    ClassLoader cl = null;
    try {
      cl = Thread.currentThread().getContextClassLoader();
    } catch (Throwable ex) {
      LoggerFactory.getLogger(EdContext.class).debug("Thread context ClassLoader unavailable.");
    }
    if (cl == null) {
      cl = EdContext.class.getClassLoader();
    }
    return cl;
  }

  /**
   * Get the default environment for all ED directory client objects created by the factory.
   *
   * @return  Production directory environment.
   */
  public static DirectoryEnv getDefaultEnvironment()
  {
    return DirectoryEnv.PROD;
  }

  /**
   * Creates an ed client object capable of interacting with the specified <em>production</em> directory.
   *
   * @param  type  Directory class type, e.g. EdAuth, EdId, EdLite.
   *
   * @return  An ED client object capable of performing operations on the specified directory instance.
   */
  public static EdService createEdSearchClient(final DirectoryType type)
  {
    return createEdSearchClient(type, getDefaultEnvironment());
  }


  /**
   * Creates an ed search client object capable of interacting with the specified directory.
   *
   * @param  type  Directory class type, e.g. EdAuth, EdId, EdLite.
   * @param  env  Directory environment, e.g. dev, pprd, prod.
   *
   * @return  An ED client object capable of performing operations on the specified directory instance.
   */
  public static EdService createEdSearchClient(final DirectoryType type, final DirectoryEnv env)
  {
    final EdServiceTemplate client = new EdServiceTemplate(type, env);
    client.initialize();
    return client;
  }


  /**
   * Creates an edauth client object capable of authentication and authorization operations on a <em>production</em>
   * directory.
   *
   * @return  An authentication/authorization-capable ED client.
   */
  public static EdAuthService createEdAuthClient()
  {
    return createEdAuthClient(getDefaultEnvironment());
  }


  /**
   * Creates an edauth client object capable of authentication and authorization operations on the specified directory.
   *
   * @param  env  Directory environment, e.g. dev, pprd, prod.
   *
   * @return  An authentication/authorization-capable ED client.
   */
  public static EdAuthService createEdAuthClient(final DirectoryEnv env)
  {
    return new EdAuth(env);
  }

  /**
   * Creates a login client object capable of authentication and authorization operations on the login directory.
   *
   * @param  env  Directory environment, e.g. dev, pprd, prod.
   *
   * @return  An authentication/authorization-capable ED client.
   */
  public static EdAuthService createLoginClient(final DirectoryEnv env)
  {
    return new Login(env);
  }

  /**
   * Gets the ED directory host name for the given directory type and environment.
   *
   * @param  type  Directory class type, e.g. EdAuth, EdId, EdLite.
   * @param  env  Directory environment, e.g. dev, pprd, prod.
   *
   * @return  Fully-qualified DNS host name of requested directory.
   */
  protected static String getLdapUrl(final DirectoryType type, final DirectoryEnv env)
  {
    return HOST_MAP.get(getTypeEnvName(type, env));
  }


  /**
   * Creates a connection config object with configuration options set for the specified directory.
   *
   * @param  type  Directory class type, e.g. EdAuth, EdId, EdLite.
   * @param  env  Directory environment, e.g. dev, pprd, prod.
   *
   * @return  connection config object configured for the specified directory instance.
   */
  public static ConnectionConfig createConnectionConfig(final DirectoryType type, final DirectoryEnv env)
  {

    final ConnectionConfig connConfig = new ConnectionConfig(getLdapUrl(type, env));

    if (DirectoryType.EDID.equals(type)) {

      final BindConnectionInitializer bci = new BindConnectionInitializer();
      bci.setBindSaslConfig(SaslConfig.builder().mechanism(Mechanism.EXTERNAL).build());
      connConfig.setUseStartTLS(true);
      connConfig.setConnectionInitializers(bci);

      final String edidSslType = ED_PROPERTIES.getProperty(
        "edldap.edid.ssl.type",
        DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.type"));

      if ("X509".equals(edidSslType.trim().toUpperCase())) {

        final String authenticationCertificatePath = ED_PROPERTIES.getProperty(
          "edldap.edid.ssl.authenticationCertificatePath",
          DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.authenticationCertificatePath"));
        final String authenticationCertificateKeyPath = ED_PROPERTIES.getProperty(
          "edldap.edid.ssl.authenticationCertificateKeyPath",
          DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.authenticationCertificateKeyPath"));
        final String trustCertificatesPath = ED_PROPERTIES.getProperty(
          "edldap.edid.ssl.trustCertificatesPath",
          DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.trustCertificatesPath"));

        connConfig.setSslConfig(
          new SslConfig(
            new EdX509CredentialConfigTemplate(
              authenticationCertificatePath,
              authenticationCertificateKeyPath,
              trustCertificatesPath)));

      } else {

        final String edidKeystorePath = ED_PROPERTIES.getProperty(
          "edldap.edid.ssl.keystorePath",
          DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.keystorePath"));
        final String edidTruststorePath = ED_PROPERTIES.getProperty(
          "edldap.edid.ssl.truststorePath",
          DEFAULT_ED_PROPERTIES.get("edldap.edid.ssl.truststorePath"));

        connConfig.setSslConfig(
          new SslConfig(new EdKeyStoreCredentialConfigTemplate(edidTruststorePath, edidKeystorePath)));

      }

      if (DirectoryEnv.LOCAL.equals(env)) {
        connConfig.getSslConfig().setHostnameVerifier(new AllowAnyHostnameVerifier());
      }
    } else if (DirectoryType.EDAUTH.equals(type) || DirectoryType.LOGIN.equals(type)) {
      connConfig.setUseStartTLS(true);

      final String edauthSslType = ED_PROPERTIES.getProperty(
        "edldap.edauth.ssl.type",
        DEFAULT_ED_PROPERTIES.get("edldap.edauth.ssl.type"));

      if ("X509".equals(edauthSslType.trim().toUpperCase())) {

        final String trustCertificatesPath = ED_PROPERTIES.getProperty(
          "edldap.edauth.ssl.trustCertificatesPath",
          DEFAULT_ED_PROPERTIES.get("edldap.edauth.ssl.trustCertificatesPath"));

        connConfig.setSslConfig(new SslConfig(new EdX509CredentialConfigTemplate(null, null, trustCertificatesPath)));

      } else {

        final String edidTruststorePath = ED_PROPERTIES.getProperty(
          "edldap.edauth.ssl.truststorePath",
          DEFAULT_ED_PROPERTIES.get("edldap.edauth.ssl.truststorePath"));

        connConfig.setSslConfig(new SslConfig(new EdKeyStoreCredentialConfigTemplate(edidTruststorePath, null)));
      }

      if (DirectoryEnv.LOCAL.equals(env)) {
        connConfig.getSslConfig().setHostnameVerifier(new AllowAnyHostnameVerifier());
      }
    }
    return connConfig;
  }

  /**
   * Creates a unique name of the form TYPE-ENV using the given directory type and environment.
   *
   * @param  type  Directory type.
   * @param  env  Directory environment.
   *
   * @return  Unique name for the directory type/environment combination given of the form TYPE-ENV.
   */
  protected static String getTypeEnvName(final DirectoryType type, final DirectoryEnv env)
  {
    return String.format("%s-%s", type.name(), env.name());
  }
}
