/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import edu.vt.middleware.ldap.ed.beans.VirginiaTechPerson;
import org.ldaptive.ConnectionFactory;
import org.ldaptive.Credential;
import org.ldaptive.DefaultConnectionFactory;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.auth.AuthenticationRequest;
import org.ldaptive.auth.AuthenticationResponse;
import org.ldaptive.auth.Authenticator;
import org.ldaptive.auth.SearchDnResolver;
import org.ldaptive.auth.SimpleBindAuthenticationHandler;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * Provides methods to perform common authentication and authorization operations.
 *
 * @author  Middleware Services
 */
abstract class AbstractEdAuthService implements EdAuthService
{

  /** Directory environment. */
  private DirectoryEnv environment;

  /** Authentication configuration. */
  private Authenticator auth;

  /** Connection factory. */
  private ConnectionFactory connectionFactory;

  @Override
  public final DirectoryEnv getEnvironment()
  {
    return environment;
  }

  @Override
  public ConnectionFactory getConnectionFactory()
  {
    return connectionFactory;
  }

  /**
   * Sets the directory environment this ED client is configured to operate on.
   *
   * @param  dirEnv  Directory environment.
   */
  protected void setEnvironment(final DirectoryEnv dirEnv)
  {
    environment = dirEnv;
  }


  /**
   * Initialize the EdAuth client for use.
   *
   * <p>This method <b>must</b> be called after the environment ( {@link #setEnvironment(DirectoryEnv)}) is
   * set and before any LDAP operations are performed. If the directory type and/or environment are changed, the search
   * client must be re-initialized for the changes to take effect on the next LDAP operation.</p>
   */
  protected final void initialize()
  {
    /*
     * Non pooled connection factory.
     */
    connectionFactory = new DefaultConnectionFactory(
      EdContext.createConnectionConfig(getType(), environment));
    // use a search dn resolver
    final SearchDnResolver dnResolver = new SearchDnResolver(connectionFactory);
    dnResolver.setBaseDn(EdContext.PEOPLE_BASE_DN);
    dnResolver.setUserFilter(EdContext.USER_FILTER);
    // perform a bind for password validation
    final SimpleBindAuthenticationHandler authHandler = new SimpleBindAuthenticationHandler(connectionFactory);
    auth = new Authenticator(dnResolver, authHandler);
  }


  @Override
  public final LdapEntry authenticate(
    final String user,
    final Credential credential,
    final String... attributes)
    throws LdapException
  {
    final AuthenticationResponse response = auth.authenticate(new AuthenticationRequest(user, credential, attributes));
    if (response.isSuccess()) {
      return response.getLdapEntry();
    } else {
      throw new LdapException(response);
    }
  }


  /**
   * {@inheritDoc}
   *
   * This method implements authorizationExpression via {@link SpelExpressionParser}.
   *
   * Available methods are authorizationExpressions are:<br>
   *                                  hasAttributeValue(String attribute, String value) and hasAttribute(String
   *                                  attribute). If the entire expression evaluates to false authorization will fail
   *                                  with EdAuthAuthorizationException. The following is an example authorization
   *                                  expression: <b>(hasAttributeValue('eduPersonAffiliation', 'VT-ACTIVE-MEMBER')
   * &amp;&amp;
   *                                  hasAttributeValue('eduPersonAffiliation', 'VT-EMPLOYEE')) ||
   *                                  hasAttribute('eduPersonPrimaryAffiliation' )</b>
   */
  @Override
  public final void authorize(final LdapEntry entry, final String authorizationExpression)
    throws LdapException, EdAuthAuthorizationException
  {
    final Map<String, String[]> attributesAndValues = new HashMap<>();
    // Return all user attributes
    Arrays.stream(entry.getAttributeNames()).forEach(userAttributeName -> attributesAndValues.put(
      userAttributeName.trim().toLowerCase(),
      entry.getAttribute(userAttributeName).getStringValues().toArray(new String[0])));
    if (authorizationExpression.trim().isEmpty()) {
      throw new EdAuthAuthorizationException(EdAuthAuthorizationException.EDAUTH_EXCEPTION_MSG_KEYSET_EMPTY);
    }
    final EdAuthorizationContext authorizationContext = new EdAuthorizationContext(attributesAndValues);
    try {
      final ExpressionParser parser = new SpelExpressionParser();
      final Expression exp = parser.parseExpression(authorizationExpression);
      final EvaluationContext context = new StandardEvaluationContext(authorizationContext);
      if (!exp.getValue(context, Boolean.class)) {
        throw new EdAuthAuthorizationException(EdAuthAuthorizationException.EDAUTH_EXCEPTION_MSG_AUTHZ_FAILED);
      }
    } catch (EvaluationException | ParseException ee) {
      throw new EdAuthAuthorizationException(EdAuthAuthorizationException.EDAUTH_EXCEPTION_MSG_AUTHZ_EXPR_FAILED, ee);
    }
  }


  @Override
  public final VirginiaTechPerson getVirginiaTechPerson(final LdapEntry result)
  {
    final VirginiaTechPerson vtPerson = new VirginiaTechPerson();
    EdServiceTemplate.MAPPER.map(result, vtPerson);
    return vtPerson;
  }


  @Override
  public final String[] getAffiliations(final LdapEntry result)
  {
    return result.getAttribute(EdContext.AFFILIATION_ATTR).getStringValues().toArray(new String[0]);
  }


  @Override
  public final String[] getGroupMembership(final LdapEntry result)
  {
    return result.getAttribute(EdContext.GROUP_MEMBERSHIP_ATTR).getStringValues().toArray(new String[0]);
  }


  /** SpEL Authorization Context for this class. */
  private static final class EdAuthorizationContext
  {

    /** Map representing the attributes and their values. */
    private final Map<String, String[]> entryAttributesAndValues;


    /**
     * Constructor must have the authorization attributes map.
     *
     * @param  attributeValuesMap  attributeValuesMap
     */
    EdAuthorizationContext(final Map<String, String[]> attributeValuesMap)
    {
      entryAttributesAndValues = attributeValuesMap;
    }


    /**
     * Whether the entry has an attribute specified.
     *
     * @param  attribute  String value of the attribute name (Case insensitive)
     *
     * @return  boolean
     */
    public boolean hasAttribute(final String attribute)
    {
      return entryAttributesAndValues
        .keySet()
        .stream()
        .anyMatch(attributeName -> attribute.trim().toLowerCase().equals(attributeName));
    }


    /**
     * Whether the entry has an attribute and the value specified.
     *
     * @param  attribute  String value of the attribute name (Case insensitive)
     * @param  attributeValue  String value of the attribute (Case sensitive)
     *
     * @return  boolean
     */
    public boolean hasAttributeValue(final String attribute, final String attributeValue)
    {
      return entryAttributesAndValues
        .entrySet()
        .stream()
        .filter(mapEntry -> attribute.trim().toLowerCase().equals(mapEntry.getKey()))
        .collect(Collectors.toList())
        .stream()
        .anyMatch(mapEntry -> Arrays.asList(mapEntry.getValue()).contains(attributeValue));
    }
  }
}
