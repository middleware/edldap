/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;


/**
 * Authorization exception thrown by {@link edu.vt.middleware.ldap.ed.EdAuth#authorize(org.ldaptive.LdapEntry, String)}.
 *
 * @author  Middleware Services
 */
public class EdAuthAuthorizationException extends Exception
{

  /** Error message for authorization failure. */
  public static final String EDAUTH_EXCEPTION_MSG_AUTHZ_FAILED =
    "Could not match all attributes requested for authorization.";

  /** Error message for a mistyped authorization expression string. */
  public static final String EDAUTH_EXCEPTION_MSG_AUTHZ_EXPR_FAILED = "Could not evaluate authorization expression.";

  /** Error message for trying to authorize with an empty authorization. */
  public static final String EDAUTH_EXCEPTION_MSG_KEYSET_EMPTY = "Authorization expression cannot be empty.";

  /** serialVersionUID. */
  private static final long serialVersionUID = -6212572629038218021L;

  /** Creates a new EdAuthAuthorization exception. */
  public EdAuthAuthorizationException() {}

  /**
   * Creates a new EdAuthAuthorization exception.
   *
   * @param  message  describing this exception
   */
  public EdAuthAuthorizationException(final String message)
  {
    super(message);
  }


  /**
   * Creates a new EdAuthAuthorization exception.
   *
   * @param  message  describing this exception
   * @param  cause  root cause
   */
  public EdAuthAuthorizationException(final String message, final Throwable cause)
  {
    super(message, cause);
  }


  /**
   * Creates a new EdAuthAuthorization exception.
   *
   * @param  cause  root cause
   */
  public EdAuthAuthorizationException(final Throwable cause)
  {
    super(cause);
  }
}
