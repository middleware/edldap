/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import edu.vt.middleware.ldap.ed.EdAuthService;
import edu.vt.middleware.ldap.ed.EdContext;
import org.ldaptive.Credential;
import org.ldaptive.DnParser;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Base class to provide drop in authentication provider for spring-security 3.2.x.
 *
 * @author  Middleware Services
 */
public abstract class AbstractEdAuthAuthenticationProvider implements AuthenticationProvider
{

  /** Class logger instance. */
  protected final Logger logger = LoggerFactory.getLogger(getClass());

  /** EdAuth service to perform authentication. */
  private final EdAuthService auth;

  public AbstractEdAuthAuthenticationProvider(final EdAuthService service)
  {
    auth = service;
  }

  @Override
  public Authentication authenticate(final Authentication authentication)
  {
    if (authentication.getPrincipal() != null && !(authentication.getPrincipal() instanceof String)) {
      throw new AuthenticationServiceException("EdAuthAuthenticationProvider only supports String principals.");
    }
    if (authentication.getCredentials() != null && !(authentication.getCredentials() instanceof String)) {
      throw new AuthenticationServiceException("EdAuthAuthenticationProvider only supports String credentials.");
    }

    final String username = (String) authentication.getPrincipal();
    final String password = (String) authentication.getCredentials();
    final LdapEntry userEntry;
    try {
      userEntry =
        auth.authenticate(username, new Credential(password), EdContext.AFFILIATION_ATTR,
          EdContext.GROUP_MEMBERSHIP_ATTR);

      // add EdAuth specific affiliations to roles
      final List<String> userRoles = new ArrayList<>();
      final String[] affil = auth.getAffiliations(userEntry);
      if (affil != null) {
        Collections.addAll(userRoles, affil);
      }

      // add EdAuth specific group membership to roles
      final String[] groupMembership = auth.getGroupMembership(userEntry);
      if (groupMembership != null) {
        Arrays.stream(groupMembership).forEach(
          groupMember -> userRoles.add(DnParser.getValue(groupMember, EdContext.UUGID_ATTR)));
      }

      final List<GrantedAuthority> authorities = new ArrayList<>();
      userRoles.forEach(userRole -> authorities.add(new SimpleGrantedAuthority(userRole)));
      return new UsernamePasswordAuthenticationToken(username, "[PROTECTED]", authorities);
    } catch (LdapException e) {
      throw new BadCredentialsException("Could not authenticate to EdAuth.", e);
    }
  }

  @Override
  public boolean supports(final Class<?> authentication)
  {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }
}
