/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.jetty.realm;

import edu.vt.middleware.ldap.ed.DirectoryEnv;
import edu.vt.middleware.ldap.ed.Login;

/**
 * {@link LoginRealm} to provide drop in authentication against Login using Jetty security realms.
 *
 * @author  Middleware Services
 */
public class LoginRealm extends AbstractEdAuthRealm
{

  /**
   * Creates a new instance of an LoginRealm object that can provide authentication against Login for the production
   * environment.
   */
  public LoginRealm()
  {
    super(new Login(), "LoginRealm");
  }

  /**
   * Creates a new instance of an LoginRealm object that can provide authentication against Login for the given
   * environment.
   *
   * @param  env  Directory environment, e.g. LOCAL|DEV|PPRD|PROD.
   */
  public LoginRealm(final DirectoryEnv env)
  {
    super(new Login(env), "LoginRealm");
  }
}
