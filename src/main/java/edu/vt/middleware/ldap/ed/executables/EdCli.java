/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed.executables;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import edu.vt.middleware.ldap.ed.DirectoryEnv;
import edu.vt.middleware.ldap.ed.DirectoryType;
import edu.vt.middleware.ldap.ed.EdAuthService;
import edu.vt.middleware.ldap.ed.EdContext;
import edu.vt.middleware.ldap.ed.EdService;
import edu.vt.middleware.ldap.ed.MultiFactor;
import edu.vt.middleware.ldap.ed.MultiFactorType;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.ldaptive.Credential;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.SearchResponse;
import org.ldaptive.io.LdifWriter;
import org.ldaptive.io.SearchResultWriter;

/**
 * Provides a command line interface to ED operations.
 *
 * @author  Middleware Services
 */
public class EdCli
{

  /** Help option flag. */
  public static final String HELP_OPTION = "h";

  /** Help long option flag. */
  public static final String HELP_LONG_OPTION = "help";

  /** Directory type option. */
  public static final String TYPE_OPTION = "type";

  /** Directory environment option. */
  public static final String ENV_OPTION = "env";

  /** Query option flag. */
  public static final String QUERY_OPTION = "query";

  /** Authenticate option flag. */
  public static final String AUTHN_OPTION = "authenticate";

  /** Use MFA option flag. */
  public static final String MFA_OPTION = "mfa";

  /** Use MFA value option flag. */
  public static final String MFA_VALUE_OPTION = "mfaval";

  /** Authorize option flag. */
  public static final String AUTHZ_OPTION = "authorize";

  /** User option flag. */
  public static final String USER_OPTION = "user";

  /** Credential option flag. */
  public static final String CREDENTIAL_OPTION = "credential";

  /** Show affiliations option flag. */
  public static final String SHOW_AFFILIATIONS_OPTION = "showaffils";

  /** Show group membership option flag. */
  public static final String SHOW_GROUP_MEMBERSHIP_OPTION = "showgroups";

  /** Commons-CLI options. */
  private final Options options;


  /** Creates a new instance able to handle command line operations. */
  public EdCli()
  {
    options = new Options();
    options.addOption(HELP_OPTION, HELP_LONG_OPTION, false, "display this help listing");
    options.addOption(ENV_OPTION, true, "LOCAL|DEV|PPRD|PROD");
    options.addOption(TYPE_OPTION, true, "EDAUTH|EDID|EDLITE");
    options.addOption(QUERY_OPTION, true, "Execute LDAP query");
    options.addOption(AUTHN_OPTION, false, "Authenticate user");
    options.addOption(MFA_OPTION, true, "Multi-factor source [DUO, TOKENS]");
    options.addOption(MFA_VALUE_OPTION, true, "Multi-factor value (For DUO: '[factor],[index|value]' " +
            "Unused sections may be omitted. Examples: 'passcode,343342' or 'phone,2' or 'push' or 'auto')");
    options.addOption(SHOW_AFFILIATIONS_OPTION, false, "Authenticate user and show affiliations");
    options.addOption(SHOW_GROUP_MEMBERSHIP_OPTION, false, "Authenticate user and show group membership");

    final Option authzOpt = new Option(AUTHZ_OPTION, true, "Authenticate and authorize user by SpEL expression");
    options.addOption(authzOpt);
    options.addOption(USER_OPTION, true, "Authentication username");
    options.addOption(CREDENTIAL_OPTION, true, "Authentication credential");
  }


  /**
   * Gets the Commons CLI options available for this command line client.
   *
   * @return  Available options.
   */
  public Options getOptions()
  {
    return options;
  }


  /**
   * Executes a query specified on the command line and displays the requested attributes on standard out.
   *
   * @param  cmd  Command line.
   *
   * @throws  Exception  On any failure
   */
  public void query(final CommandLine cmd)
    throws Exception
  {
    final EdService client = EdContext.createEdSearchClient(getType(cmd), getEnv(cmd));
    final String query = cmd.getOptionValue(QUERY_OPTION);
    System.err.println("Searching " + client.getType().name());
    System.err.println("Query: " + query);

    final SearchResponse results;
    if (cmd.getArgs() == null || cmd.getArgs().length == 0) {
      results = client.search(query, (String[]) null);
    } else {
      results = client.search(query, cmd.getArgs());
    }

    final SearchResultWriter writer = new LdifWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

    writer.write(results);


  }


  /**
   * Authenticates the user specified on the command line. If no credential was supplied on the command line, it is
   * prompted to be read from standard input.
   *
   * @param  cmd  Command line.
   * @param  attributes Attributes to return
   *
   * @return Found {@link LdapEntry}
   *
   * @throws  Exception  On any failure
   */
  public LdapEntry authenticate(final CommandLine cmd, final String... attributes)
    throws Exception
  {
    final EdAuthService client = configureEdAuthClient(cmd);
    final String user = getUser(cmd);
    System.err.println("Performing authentication using " + client.getType().name());
    System.err.println("User: " + user);

    try {
      final LdapEntry entry = client.authenticate(user, getCredential(cmd), attributes);
      System.out.println("SUCCESS.  Authenticated " + user);
      return entry;
    } catch (LdapException edAuthException) {
      System.out.println("FAILURE.  Could not authenticate " + user);
      throw edAuthException;
    }
  }


  /**
   * Authenticates and authorizes the user using command line options. If no credential was supplied on the command
   * line, it is prompted to be read from standard input.
   *
   * @param  cmd  Command line.
   * @param entry {@link LdapEntry} of authenticated use information, may be null
   *
   * @throws  Exception  On any failure
   */
  public final void authorize(final CommandLine cmd, final LdapEntry entry)
    throws Exception
  {
    final EdAuthService client = configureEdAuthClient(cmd);
    final String user = getUser(cmd);
    final String authzExpression = cmd.getOptionValue(AUTHZ_OPTION);
    System.err.println("Performing authorization using " + client.getType().name());
    System.err.println("User: " + user);

    try {
      if (entry == null) {
        client.authorize(authenticate(cmd, EdContext.ALL_ATTR), authzExpression);
      } else {
        client.authorize(entry, authzExpression);
      }
      System.out.println("SUCCESS.  Authenticated and authorized " + user);
    } catch (LdapException edAuthException) {
      System.out.println("FAILURE.  Could not authenticate and authorize " + user);
      throw edAuthException;
    }
  }


  /**
   * Authenticates the user specified on the command line and displays their group membership. If no credential was
   * supplied on the command line, it is prompted to be read from standard input.
   *
   * @param  cmd  Command line.
   * @param entry {@link LdapEntry} of authenticated use information, may be null
   *
   * @throws  Exception  On any failure
   */
  public void displayGroupMembership(final CommandLine cmd, final LdapEntry entry)
    throws Exception
  {
    final EdAuthService client = configureEdAuthClient(cmd);
    final String user = getUser(cmd);
    System.err.println("Performing group membership operation using " + client.getType().name());
    System.err.println("User: " + user);
    final String[] membership;
    if (entry == null) {
      membership = client.getGroupMembership(authenticate(cmd, EdContext.GROUP_MEMBERSHIP_ATTR));
    } else {
      membership = client.getGroupMembership(entry);
    }
    System.out.println("Group Membership: ");
    Arrays.stream(membership).forEach(aMembership -> System.out.println("  " + aMembership));
  }


  /**
   * Authenticates the user specified on the command line and displays their affiliations. If no credential was supplied
   * on the command line, it is prompted to be read from standard input.
   *
   * @param  cmd  Command line.
   * @param entry {@link LdapEntry} of authenticated use information, may be null
   *
   * @throws  Exception  On any failure
   */
  public void displayAffiliations(final CommandLine cmd, final LdapEntry entry)
    throws Exception
  {
    final EdAuthService client = configureEdAuthClient(cmd);
    final String user = getUser(cmd);
    System.err.println("Performing affiliation operation using " + client.getType().name());
    System.err.println("User: " + user);

    final String[] affiliations;
    if (entry == null) {
      affiliations = client.getAffiliations(authenticate(cmd, EdContext.AFFILIATION_ATTR));
    } else {
      affiliations = client.getAffiliations(entry);
    }
    System.out.println("Affiliations: ");
    Arrays.stream(affiliations).forEach(affiliation -> System.out.println("  " + affiliation));
  }


  /**
   * Application entry point for command line operations.
   *
   * @param  args  command line arguments
   *
   * @throws  Exception  if an error occurs
   */
  public static void main(final String[] args)
    throws Exception
  {
    final EdCli edCmdLine = new EdCli();
    final CommandLineParser parser = new DefaultParser();
    final CommandLine cmd = parser.parse(edCmdLine.getOptions(), args);
    if (cmd.hasOption(HELP_OPTION) || cmd.getOptions().length == 0) {
      final HelpFormatter hf = new HelpFormatter();
      hf.printHelp("edldap", edCmdLine.getOptions());
    } else if (cmd.hasOption(QUERY_OPTION)) {
      edCmdLine.query(cmd);
    } else {
      final LdapEntry userEntry = getLdapEntry(cmd, edCmdLine);
      if (userEntry != null) {
        if (cmd.hasOption(AUTHZ_OPTION)) {
          edCmdLine.authorize(cmd, userEntry);
        }
        if (cmd.hasOption(SHOW_AFFILIATIONS_OPTION)) {
          edCmdLine.displayAffiliations(cmd, userEntry);
        }
        if (cmd.hasOption(SHOW_GROUP_MEMBERSHIP_OPTION)) {
          edCmdLine.displayGroupMembership(cmd, userEntry);
        }
      }
    }
  }


  /**
   * Unifies requests into a single authentication call to the ldap instance, returning attributes in hierarchical
   * complexity.
   *
   * @param cmd Command line
   * @param edCmdLine {@link EdCli} instance
   * @return {@link LdapEntry} requested, null if nothing was requested
   *
   * @throws Exception if an error occurs
   */
  private static LdapEntry getLdapEntry(final CommandLine cmd, final EdCli edCmdLine) throws Exception
  {
    final LdapEntry userEntry;
    if (cmd.hasOption(AUTHZ_OPTION)) {
      userEntry = edCmdLine.authenticate(cmd, EdContext.ALL_ATTR);
    } else if (cmd.hasOption(SHOW_AFFILIATIONS_OPTION) && cmd.hasOption(SHOW_GROUP_MEMBERSHIP_OPTION)) {
      userEntry = edCmdLine.authenticate(cmd, EdContext.GROUP_MEMBERSHIP_ATTR, EdContext.AFFILIATION_ATTR);
    } else if (cmd.hasOption(SHOW_AFFILIATIONS_OPTION)) {
      userEntry = edCmdLine.authenticate(cmd, EdContext.AFFILIATION_ATTR);
    } else if (cmd.hasOption(SHOW_GROUP_MEMBERSHIP_OPTION)) {
      userEntry = edCmdLine.authenticate(cmd, EdContext.GROUP_MEMBERSHIP_ATTR);
    } else if (cmd.hasOption(AUTHN_OPTION)) {
      userEntry = edCmdLine.authenticate(cmd);
    } else {
      userEntry = null;
    }
    return userEntry;
  }


  /**
   * Gets the directory type specified on the command line.
   *
   * @param  cmd  Command line.
   *
   * @return  Specified type or ED-Lite type if none was specified.
   */
  private static DirectoryType getType(final CommandLine cmd)
  {
    final String type = cmd.getOptionValue(TYPE_OPTION, DirectoryType.EDLITE.name());
    return DirectoryType.valueOf(DirectoryType.class, type);
  }


  /**
   * Gets the directory environment specified on the command line.
   *
   * @param  cmd  Command line.
   *
   * @return  Specified environment or PROD env if none was specified.
   */
  private static DirectoryEnv getEnv(final CommandLine cmd)
  {
    final String env = cmd.getOptionValue(ENV_OPTION, DirectoryEnv.PROD.name());
    return DirectoryEnv.valueOf(DirectoryEnv.class, env);
  }


  /**
   * Gets the MFA type, returns null if it does not exist.
   *
   * @param  cmd  Command line
   *
   * @return  MFA type, null otherwise
   */
  private static String getMFA(final CommandLine cmd)
  {
    return cmd.getOptionValue(MFA_OPTION);
  }


  /**
   * Gets the MFA value, returns null if it does not exist.
   *
   * @param  cmd  Command line
   *
   * @return  MFA value, null otherwise
   */
  private static String getMFAValue(final CommandLine cmd)
  {
    return cmd.getOptionValue(MFA_VALUE_OPTION);
  }


  /**
   * Gets the authentication credential specified on the command line, or prompts for it on standard input if none was
   * specified.  The credentials are supplied with MFA value if appropriate.
   *
   * @param  cmd  Command line
   *
   * @return  Authentication {@link Credential}
   *
   * @see EdCli#getCredentialWithMFA(org.apache.commons.cli.CommandLine, org.ldaptive.Credential)
   */
  private static Credential getCredential(final CommandLine cmd)
  {
    final String password;
    if (cmd.hasOption(CREDENTIAL_OPTION)) {
      password = cmd.getOptionValue(CREDENTIAL_OPTION);
    } else {
      final char[] pass = System.console().readPassword(
        String.format("Enter password for user %s:", cmd.getOptionValue(USER_OPTION)));
      password = String.valueOf(pass);
    }
    return getCredentialWithMFA(cmd, new Credential(password));
  }


  /**
   * Returns {@link Credential} instance containing the MFA value.
   *
   * @param cmd Command line
   * @param credentials Credentials to append MFA to
   *
   * @return {@link Credential} instance containing the MFA value
   *
   * @throws IllegalArgumentException If an unknown MFA source is specified or MFA value is not fulfilled
   */
  private static Credential getCredentialWithMFA(
          final CommandLine cmd, final Credential credentials) throws IllegalArgumentException
  {
    final String mfa = getMFA(cmd);
    if (mfa == null) {
      return credentials;
    }
    return MultiFactor.getInstance(MultiFactorType.valueOf(mfa), getMFAValue(cmd)).getValue(credentials);
  }


  /**
   * Gets the user specified on the command line.
   *
   * @param  cmd  Command line.
   *
   * @return  User specified on command line.
   *
   * @throws  MissingOptionException  If no user option is found.
   */
  private static String getUser(final CommandLine cmd)
    throws MissingOptionException
  {
    if (!cmd.hasOption(USER_OPTION)) {
      throw new MissingOptionException("user option is required for authorization.");
    }
    return cmd.getOptionValue(USER_OPTION);
  }


  /**
   * Returns an {@link EdAuthService} instance specified by optional command-line arguments.
   *
   * @param  cmd  Command line.
   *
   * @return  {@link EdAuthService} instance
   */
  private EdAuthService configureEdAuthClient(final CommandLine cmd)
  {
    final String mfa = getMFA(cmd);
    final EdAuthService client;
    if (mfa != null) {
      client = EdContext.createLoginClient(getEnv(cmd));
    } else {
      client = EdContext.createEdAuthClient(getEnv(cmd));
    }
    return client;
  }
}
