/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

/**
 * Represents types of multi-factor authentication supported by this library
 *
 * <ul>
 *   <li>DUO</li>
 *   <li>TOKENS</li>
 * </ul>
 *
 * @author  Middleware Services
 */
public enum MultiFactorType
{

  /** Direct DUO integration with your second factor. */
  DUO,

  /** Multi-factor using ED Web Services /tokens endpoint.  */
  TOKENS,

}
