/* See LICENSE for licensing and NOTICE for copyright. */
package edu.vt.middleware.ldap.ed;

/**
 * Represents a class of the Enterprise Directory:
 *
 * <ul>
 *   <li>ED Lite</li>
 *   <li>ED ID</li>
 *   <li>ED Auth</li>
 *   <li>Login</li>
 * </ul>
 *
 * @author  Middleware Services
 */
public enum DirectoryType {

  /** ED Lite directory. */
  EDLITE,


  /** ED Auth directory. */
  EDAUTH,


  /** ED ID directory. */
  EDID,

  /** Login directory. */
  LOGIN
}
